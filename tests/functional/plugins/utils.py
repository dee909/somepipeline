from somepipeline.plugins import Plugin


#
#   Plugin registry tools
#
def clear_plugin_registry(registry, plugin_category: str | None = None):
    """
    Clear a plugin category in the registry.
    If `plugin_category` is None, every category is cleared.
    """
    if plugin_category:
        registry._plugins[plugin_category].clear()
    else:
        registry._plugins.clear()


def add_mockup_plugins(registry, **category_to_plugin_type_names: dict[str, list[str]]):
    category_plugins = {}
    for category in category_to_plugin_type_names.keys():

        @classmethod
        def plugin_category(cls):
            return category

        plugin_category_class = type(
            category + "Plugin", (Plugin,), {"plugin_category": plugin_category}
        )
        category_plugins[category] = plugin_category_class

    for category, plugin_type_names in category_to_plugin_type_names.items():
        base = category_plugins[category]
        for plugin_type_name in plugin_type_names:
            plugin_class = type(plugin_type_name, (base,), {})
            registry.add_plugin(plugin_class)
