from __future__ import annotations
from typing import TYPE_CHECKING, Protocol
from collections.abc import Generator
import enum
import logging
import contextlib
import traceback

if TYPE_CHECKING:
    from .asset_plugin import Asset

import pydantic


class OperationContext:
    """
    The operation context brings info and tools to
    the asset's `build()`, `check()`, and `export()`
    operations.

    The context can be updated during operations in order
    to pass info and tools along to the next assets in
    the process.
    """

    def __init__(self, operation: str) -> None:
        self.operation = operation

    def pformat(self, indent_level=0):
        indent_str = indent_level * "  "
        lines = [f"{indent_str}{key}={value!r}" for key, value in self.__dict__.items()]
        return "\n".join(lines)


class ReportEntry:
    def __init__(self, level: OperationReport.LogLevel, content):
        self.level = level
        self.content = content

    def update_level(self):
        try:
            level = self.content.level
        except AttributeError:
            pass
        else:
            self.level = level

    def pformat(self, indent_level=0):
        try:
            formated = self.content.pformat(indent_level + 1)
        except (AttributeError, TypeError):
            return (indent_level * "  ") + str(self.content)
        else:
            return formated


class ReportActionArgs(pydantic.BaseModel):
    """
    Base for input requested fo a Report Action.

    User adding an Action to a report will want to define a
    model for its args. UX may present the modl to the user
    for input or directly use- the default values.
    """

    ...


class ReportAction(Protocol):
    def __call__(self, args: ReportActionArgs | None = None) -> bool:
        """
        Signature for a Report Action.

        If your action needs args, you will want to subclasss ReportActionArgs
        to define args model and defualt values.

        """
        ...


class ActionReportEntry(ReportEntry):
    def __init__(
        self, label, action: ReportAction, args: ReportActionArgs | None = None
    ):
        super().__init__(OperationReport.LogLevel.NotSet, (action, args))


class OperationReport:
    """
    Each operation performed on an asset will produce a report.
    This is the base class for such report.
    """

    # for convenience:
    ReportActionArgs = ReportActionArgs

    class LogLevel(enum.IntEnum):
        """
        Python logging module log level constants represented as an ``enum.Enum``.
        """

        NotSet = logging.NOTSET
        Debug = logging.DEBUG
        Info = logging.INFO
        Warning = logging.WARNING
        Error = logging.ERROR
        Critical = logging.CRITICAL

    def __init__(self, title: str) -> None:
        self._title = title
        self._entries: list[ReportEntry] = []

    @property
    def title(self) -> str:
        return self._title

    def _update_levels(self):
        for entry in self._entries:
            entry.update_level()

    @property
    def level(self) -> LogLevel:
        """The report level is the highest level of all entries."""
        # We must consolidate our entries level before finding out ours:
        self._update_levels()
        return max([entry.level for entry in self._entries])

    @property
    def success(self) -> bool:
        """
        Return True if no entry has a level higher than Warning
        """
        level = self.level
        return level > self.LogLevel.NotSet and level <= self.LogLevel.Warning

    def _add_entry(self, log_level: LogLevel, content: str | OperationReport):
        self._entries.append(ReportEntry(log_level, content))

    def add_report(self, report: OperationReport):
        # NB: at the time the report is added, it's level may
        # not be settled. We need to come back later and update
        # the entry level. This is done by `_bake_entry_levels()`
        self._add_entry(self.LogLevel.NotSet, report)

    def add_action(
        self, label, action: ReportAction, args: ReportActionArgs | None = None
    ):
        self._entries.append(ActionReportEntry(label, action, args))

    def object(self, object: object):
        self._add_entry(self.LogLevel.NotSet, object)

    def debug(self, message):
        self._add_entry(self.LogLevel.Debug, message)

    def info(self, message):
        self._add_entry(self.LogLevel.Info, message)

    def warning(self, message):
        self._add_entry(self.LogLevel.Warning, message)

    def error(self, message, trace: str):
        # FIXME: shouldn't we put the trave *before* the error message ?
        self._add_entry(self.LogLevel.Error, message)
        self._add_entry(self.LogLevel.Info, trace)

    def Critical(self, message):
        self._add_entry(self.LogLevel.Critical, message)

    @contextlib.contextmanager
    def section(self, title: str) -> Generator[OperationReport, None, None]:
        section = OperationReport(title)
        self.add_report(section)
        try:
            yield section
        finally:
            # -_____-
            # TODO: Would it be enough to update the entry level
            # here, since the section is closed ?
            # => what if someone kept a pointer to the section and
            # is still adding entries to it ?
            pass

    @contextlib.contextmanager
    def asset_section(self, asset: Asset) -> Generator[OperationReport, None, None]:
        title = f"{asset.asset_name} ({asset.asset_type_name()})"
        with self.section(title) as section:
            yield section

    @contextlib.contextmanager
    def exceptions(
        self, on_error_message: str = "", *catched_exception_types: Exception
    ):
        try:
            yield
        except Exception as err:
            self.error(f"{on_error_message}{err}", traceback.format_exc())
            if type(err) not in catched_exception_types:
                raise

    def pformat(self, indent_level=0):
        indent_str = indent_level * "  "
        lines = [f"{indent_str}[{self.title}]"]
        lines += [entry.pformat(indent_level + 1) for entry in self._entries]
        return "\n".join(lines)
