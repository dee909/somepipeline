from pathlib import Path
from urllib.parse import urlparse

import pytest

from somepipeline.plugins import PluginRegistry
from somepipeline.am.asset_manager.asset_index import lan, memory
from somepipeline.am.asset_manager.asset_index.asset_index_plugin import AssetIndex


def get_default_asset_content(asset_name, version):
    return f"# This is default content for asset {asset_name} at version {version}"


def get_test_assets_info() -> (
    list[str, str, list[str], list[str | int], dict[str, str | int]]
):
    """
    Return a list of:
        asset_name, asset_type, tags, versions, flags
    """
    # ! test rely on the fact that the last version of each entry here
    # is an int:
    return (
        (
            "SETS/Kitchen/Kitchen-mod.ma",
            "SetMod",
            ["SET", "Task:Mod"],
            [1, 2, 3],
            dict(Blessed=2),
        ),
        (
            "PROPS/Kitchen/Table/Kitchen_Table-lookdef.ma",
            "Lookdev",
            ["PROPS", "KITCHEN", "Task:Lookdev"],
            [1, "1.1", "1.2", "1.2.1", "1.2.2", 2, 3, 4],
            dict(Blessed="1.2.2", Valid=3, Prev=3, too_ugly=2),
        ),
        (
            "PROPS/Kitchen/Chair/Kitchen_Chair-lookdef.ma",
            "Lookdev",
            ["PROPS", "KITCHEN", "Task:Lookdev"],
            [1, 2],
            dict(Blessed=2),
        ),
    )


def ingest(index: AssetIndex, asset_name, asset_type, tags, versions, flags):
    for version in versions:
        content = get_default_asset_content(asset_name, versions)
        index.ingest_content(
            content, asset_name, version, "content ingested for tests."
        )
    for flag, version in flags.items():
        index.flag_version(asset_name, version, flag)
    index.set_asset_type(asset_name, asset_type)
    index.add_tags(asset_name, tags)


def ingest_all(index, assets_info):
    for asset_name, asset_type, tags, versions, flags in assets_info:
        ingest(index, asset_name, asset_type, tags, versions, flags)


_PLUGIN_REGISTRIES = dict()


def plugin_registry(name):
    global _PLUGIN_REGISTRIES
    try:
        return _PLUGIN_REGISTRIES[name]
    except KeyError:
        plugin_registry = PluginRegistry()
        plugin_registry.add_plugins(lan.LANAssetIndex, memory.MemoryAssetIndex)
        _PLUGIN_REGISTRIES[name] = plugin_registry
        return plugin_registry


def get_asset_index(tmp_path, plugin_registry_name, scheme):
    # FIXME: this url contruction would not be right for every plugin !
    STORE_FOLDER = tmp_path / "ASSET_STORE"
    index_url = f"{scheme}://{STORE_FOLDER}"
    url_info = urlparse(index_url)

    asset_index_type = plugin_registry(plugin_registry_name).elect_plugin(
        "AssetIndex", url_info
    )
    asset_index = asset_index_type(index_url)

    return asset_index


@pytest.fixture(params=["lan", "memory"])
def asset_index(tmp_path, request):
    asset_index = get_asset_index(tmp_path, "LOADED", scheme=request.param)
    ingest_all(asset_index, get_test_assets_info())

    return asset_index


@pytest.fixture(params=["lan", "memory"])
def empty_asset_index(tmp_path, request):
    asset_indes = get_asset_index(tmp_path, "EMPTY", scheme=request.param)
    return asset_index
