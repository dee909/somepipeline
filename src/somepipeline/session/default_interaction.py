from typing import TYPE_CHECKING
import logging

from .interaction import InteractionPLugin, Plugin

if TYPE_CHECKING:
    from .session import Session
else:
    Session = type

logger = logging.getLogger(__name__)


class Progress:
    def __init__(
        self,
        session: Session,
        progress_id: str,
        at_step: str,
        steps: list[str],
        parent_id: str | None = None,
    ) -> None:
        self.session = session
        self.progress_id = progress_id
        self.parent_id = parent_id
        self.steps = steps
        self.at_step = at_step

    @property
    def progress_key(self):
        preffix = self.parent_id and f"{self.parent_id}/" or ""
        return f"{preffix}{self.progress_id}"

    def has_steps(self) -> bool:
        return self.steps and self.at_step in self.steps

    def perten(self) -> int:
        step_index = self.steps.index(self.at_step)
        perten = int((step_index * 10) / len(self.steps))
        return perten

    def str_bar(self) -> str:
        if self.has_steps():
            perten = self.perten()
            bar = perten * "|" + (10 - perten) * "-"
        else:
            bar = 10 * "~"
        return f"|{bar}| {self.at_step}"

    def str_header(self) -> str:
        session_name = self.session.session_name
        return f"[{session_name}::Progress] {self.progress_key}:"

    def print(self, header: bool = True):
        if header:
            print(self.str_header())
        print(self.str_bar())

    def __str__(self) -> str:
        return self.str_header() + " " + self.str_bar()


class DefaultInteraction(InteractionPLugin):
    @classmethod
    def is_plugin_candidat(cls, *args, **kwargs):
        """
        Default is always candidat.
        """
        return True

    @classmethod
    def plugin_election(
        cls,
        candidats: set[Plugin],
        *candidats_args,
        **candidats_kwargs,
    ):
        """
        Default only stay if it's the sole candidat.
        """
        if cls in candidats and candidats != {cls}:
            candidats.remove(cls)

    def __init__(self, session: Session):
        super().__init__(session)
        self._last_progress_key: str | None = None
        self._progresses: dict[str:Progress] = {}
        self._notification = []
        self._input_requests = {}

    def set_progress(
        self,
        progress_id: str,
        at_step: str,
        steps: list[str],
        parent_id: str | None = None,
    ):
        progress = Progress(self._session, progress_id, at_step, steps, parent_id)
        self._progresses[progress.progress_key] = progress

        progress.print(header=progress.progress_key != self._last_progress_key)
        self._last_progress_key = progress.progress_key

    def notify(self, level: str, title: str, message: str):
        print(f"[{self.session_name}::{level} {title}:\n{message}\n{10*'-'}")

    def request_input(self, ui: dict) -> object | None:
        lines = [f"{k}={v!r}" for k, v in dict.items()]
        print(lines)
        v = input("? >>>")
        print(v)
        print()
        return v
