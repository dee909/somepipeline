from pathlib import Path
import logging
from urllib.parse import urlparse, ParseResult
import os
import shutil
import json

from .asset_index_plugin import AssetIndex, AssetMetadata, VersionMetadata, LogEntry
from ..requirement import Requirement

logger = logging.getLogger(__name__)


class LANAssetIndex(AssetIndex):
    """
    An index with assets stored on a local area network, accessible
    thru filesystem paths.
    """

    @classmethod
    def is_plugin_candidat(cls, url_info: ParseResult):
        return url_info.scheme == "lan"

    def _setup(self, url: str, url_info: ParseResult):
        self.path = Path(url_info.netloc)
        self.username = url_info.username

    def get_flags(self, asset_name: str) -> set[str]:
        path = self.path / asset_name / Requirement.FLAG_OPERATOR
        flags = set(os.listdir(path))
        flags.add(self.DYNAMIC_FLAGS.HEAD)
        return flags

    def get_flag_version(self, asset_name: str, flag: str):
        flag_file = self.path / asset_name / Requirement.FLAG_OPERATOR / flag

        # FIXME: we shouldnt raise Exceptions from Requirement but from AssetIndex !
        if not flag_file.exists():
            Requirement.build_flag(asset_name, flag).raise_flag_not_found()
        with open(flag_file) as fp:
            version = fp.read().strip()
        return version

    def get_asset_versions(
        self,
        asset_name: str,
        require_operator: str | None = None,
        require_pattern: str | None = None,
    ):
        path = self.path / asset_name
        if require_operator is None:
            versions = [
                name
                for name in os.listdir(path)
                if (name != Requirement.FLAG_OPERATOR and not name.startswith("__"))
            ]
            # TODO: support exclusion of yanked versions
            return self.sorted_versions(versions)

        elif require_operator == "~=":
            versions = [
                name
                for name in list(path.glob(require_pattern))
                if name not in Requirement.FLAG_OPERATORS
            ]
            versions = self.sorted_versions(versions)
            # the pattern X.Y.* does not include X.Y
            # but we should include it (lower bound is >=)
            # so:
            if require_pattern.endswith(".*"):
                lower = require_pattern[:-2]
                if self.has_version(asset_name, lower):
                    versions.insert(0, lower)
            # TODO: support exclusion of yanked versions
            return versions

        else:
            raise ValueError(
                f"Unsupported asset versions filtering: {require_operator=}."
            )

    def _asset_path(self, asset_name: str, version: int | str) -> Path:
        filename = os.path.basename(asset_name)
        return self.path / asset_name / str(version) / filename

    def _asset_metadata_path(self, asset_name: str) -> Path:
        filename = os.path.basename(asset_name)
        return self.path / asset_name / "__metadata__" / (filename + ".json")

    def _write_asset_metadata(self, asset_name: str, metadata: AssetMetadata):
        md_path = self._asset_metadata_path(asset_name)
        md_path.parent.mkdir(parents=True, exist_ok=True)
        with open(md_path, "w") as fp:
            fp.write(metadata.json())

    def _read_asset_metadata(self, asset_name: str) -> AssetMetadata:
        md_path = self._asset_metadata_path(asset_name)
        if not md_path.exists():
            return AssetMetadata(asset_name=asset_name, asset_type="???")
        with open(md_path, "r") as fp:
            data = json.load(fp)
        return AssetMetadata(**data)

    def _version_metadata_path(self, asset_name: str, version: int | str) -> Path:
        path = self._asset_path(asset_name, version)
        return path.parent / (path.name + ".json")

    def _write_version_metadata(
        self, asset_name: str, version: int | str, metadata: AssetMetadata
    ):
        md_path = self._version_metadata_path(asset_name, version)
        md_path.parent.mkdir(parents=True, exist_ok=True)
        with open(md_path, "w") as fp:
            fp.write(metadata.json())

    def _read_version_metadata(
        self, asset_name: str, version: int | str
    ) -> VersionMetadata:
        # FIXME: add a contextmanager with a file lock to
        # mitigate race conditions when we read, edit, write.
        md_path = self._version_metadata_path(asset_name, version)
        if not md_path.exists():
            return VersionMetadata(
                asset_name=asset_name,
                version=version,
                description="No metadata found for this version !",
            )
        with open(md_path, "r") as fp:
            data = json.load(fp)
        return VersionMetadata(**data)

    def get_asset_metadata(self, asset_name: str) -> AssetMetadata:
        md = self._read_asset_metadata(asset_name)
        md.versions = self.get_asset_versions(asset_name)
        return md

    def set_versions_inherit_requirements(self, asset_name: str, b: bool):
        md = self._read_asset_metadata(asset_name)
        before = md.versions_inherit_requirements
        if before == b:
            self._log_in_metadata(
                md, "versions_inherit_requirements", f"Was already {b!r}, as requested."
            )
        else:
            md.versions_inherit_requirements = b
            self._log_in_metadata(
                md, "versions_inherit_requirements", f"changed from {before!r} to {b!r}"
            )
        self._write_asset_metadata(asset_name, md)

    def set_asset_type(self, asset_name: str, asset_type: str):
        md = self._read_asset_metadata(asset_name)
        before = md.asset_type
        md.asset_type = asset_type
        self._log_in_metadata(
            md, "asset_type", f"changed from {before!r} to {asset_type!r}"
        )
        self._write_asset_metadata(asset_name, md)

    def add_tags(self, asset_name: str, tags: set[str]):
        md = self._read_asset_metadata(asset_name)
        before = md.tags.copy()
        md.tags |= set(tags)
        self._log_in_metadata(
            md, "tags", f"added {tags} = changed from {before!r} to {md.tags!r}"
        )
        self._write_asset_metadata(asset_name, md)

    def remove_tags(self, asset_name: str, tags: set[str]):
        md = self._read_asset_metadata(asset_name)
        before = md.tags
        md.tags -= set(tags)
        self._log_in_metadata(md, "tags", f"changed from {before!r} to {tags!r}")
        self._write_asset_metadata(asset_name, md)

    def get_version_metadata(
        self, asset_name: str, version: int | str
    ) -> VersionMetadata:
        md = self._read_version_metadata(asset_name, version)

        # update flags value which are stored on filesystem:
        # FIXME: this is very sub optimal, maybe we should sync the metadata and use them
        # instead of relying on filesystem tricks (and accesses) to store the verions.
        md.flags = self.get_version_flags(asset_name, version)
        return md

    def set_version_description(self, asset_name: str, version: str, description: str):
        md = self._read_version_metadata(asset_name, version)
        before = md.description
        md.description = description
        self._log_in_metadata(
            md, "description", f"changed from {before!r} to {description!r}"
        )
        self._write_version_metadata(asset_name, version, md)

    def set_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._read_version_metadata(asset_name, version)
        md.requirements = set(requirements)
        self._log_in_metadata(md, "requirements", f"Set to {requirements}")
        self._write_version_metadata(asset_name, version, md)

    def add_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._read_version_metadata(asset_name, version)
        md.requirements |= set(requirements)
        self._log_in_metadata(md, "requirements", f"Added {requirements}")
        self._write_version_metadata(asset_name, version, md)

    def remove_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._read_version_metadata(asset_name, version)
        md.requirements -= set(requirements)
        self._log_in_metadata(md, "requirements", f"Removed {requirements}")
        self._write_version_metadata(asset_name, version, md)

    def _create_new_version(
        self, asset_name: str, version: int | str, description: str
    ):
        """
        Create a new version for the asset by creating its metadata using
        the current head or default values.
        """
        md = VersionMetadata(
            asset_name=asset_name,
            version=str(version),
            description=description,
        )

        # inherit some metadata from @Head, if it exists:
        log_message_lines = []
        try:
            head_version = self.get_flag_version(asset_name, self.DYNAMIC_FLAGS.HEAD)
        except Requirement.FlagNotFoundError:
            log_message_lines.append(
                f"No @Head version found to inherit metadata, using defaults."
            )
        else:
            head_md = self.get_version_metadata(asset_name, head_version)
            # TODO: if the new version is same major number, automatically bump
            # the flags too ?
            if self._read_asset_metadata(asset_name).versions_inherit_requirements:
                md.requirements = head_md.requirements.copy()
                log_message_lines.append(
                    f"Herited requirements from @Head (=={head_version})."
                )
            md.custom_data = head_md.custom_data
            log_message_lines.append(
                f"Herited custom data from @Head (=={head_version})."
            )

        self._log_in_metadata(md, "New Version", "\n".join(log_message_lines))
        self._write_version_metadata(asset_name, version, md)

    def ingest_content(
        self,
        content: str,
        asset_name: str,
        version: int | str,
        description: str,
    ):
        self._create_new_version(asset_name, version, description)

        dst = self._asset_path(asset_name, version)
        if 0:
            # not needed anymore, we use `self._create_new_version()` now.
            dst.parent.mkdir(parents=True, exist_ok=True)
        with open(dst, "w") as fp:
            fp.write(content)

        self._on_new_version_created(asset_name, version)

    def ingest_asset(
        self,
        source_path: Path,
        asset_name: str,
        version: int | str,
        description: str,
    ):
        self._create_new_version(asset_name, version, description)
        dst = self._asset_path(asset_name, version)
        self.copy_asset(source_path, dst)
        self._on_new_version_created(asset_name, version)

    def copy_asset(self, source: Path, destination: Path):
        """
        Copy anything that could be an asset from source to destination.
        """
        destination.parent.mkdir(parents=True, exist_ok=True)
        if source.is_dir():
            shutil.copytree(source, destination)
        else:
            shutil.copy(source, destination)

    def has_asset(self, asset_name: str):
        return (self.path / asset_name).exists()

    def has_version(self, asset_name: str, version: int | str):
        path = self.path / asset_name / str(version)
        return path.exists()

    def _flag_version(self, asset_name: str, version: int | str, flag: str):
        md = self.get_asset_metadata(asset_name)
        path = self.path / asset_name / Requirement.FLAG_OPERATOR / flag
        path.parent.mkdir(parents=True, exist_ok=True)
        with open(path, "w") as fp:
            fp.write(str(version))
        self._log_in_metadata(md, "flags", f"flag {flag} set to version {version}")
        self._write_asset_metadata(asset_name, md)

    def checkout_asset(
        self, asset_name: str, version: int | str, destination: Path, editable: bool
    ):
        source = self._asset_path(asset_name, version)

        checkout_path = self.checkout_path(asset_name, destination)
        checkout_dir = checkout_path.parent
        checkout_dir.mkdir(
            parents=True, exist_ok=True
        )  # this exists_ok freaks me out :'(

        self.copy_asset(source, checkout_dir)

        if not editable:
            self._set_read_only(checkout_path)

        return checkout_path

    def checkin_asset(
        self,
        asset_name: str,
        source: Path,
        description: str,
        original_version: int | str,
        bump_major: bool = False,
        bump_minor: bool = False,
        bump_micro: bool = False,
        flags: list[str] | tuple[str] | set[str] = [],
    ) -> str:
        # FIXME: ! CRITICAL: between the creation of the version and the
        # creation of the assert file or folder, one could try to checkout
        # the new version, which would be incomplete !!!
        # => Fix this by creating new version with a prefix that will hide
        # it from existing versions and rename it to the final name
        # after completing the transfert.

        return super().checkin_asset(
            asset_name,
            source,
            description,
            original_version,
            bump_major,
            bump_minor,
            bump_micro,
            flags,
        )
