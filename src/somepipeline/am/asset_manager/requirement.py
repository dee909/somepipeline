from typing import Union
import re
import logging

logger = logging.getLogger(__name__)


class Requirement(str):
    """
    Specify which version of an asset you're looking for:
    Use `==` for exact version (aka concrete requirement):
        asset_name==9
        asset_name==9.8
        asset_name==9.8.7
        ...

    Use `~=` for highest in range lookup:
        asset_name~=9.*     : >= 9 and < 10
        asset_name~=9.8.*   : >= 9.8 and < 10
        asset_name~=9.8.7   : >= 9.8.7 and < 9.9

    Use `@` for a flaged version:
        asset_name@Blessed       : The currently blessed version.
        asset_name@!!blah blah#  : Flag name can use any characters

    The version may indicate an edited state:
        asset_name==1.2.3+edit
    """

    CONCREAT_OPERATOR = "=="
    COMP_OPERATORS = ("==", "~=")
    FLAG_OPERATOR = "@"
    EDIT_SUFFIX = "+edit"
    _comp_operators_re = (
        r"(?P<operator>" + "|".join(COMP_OPERATORS + (FLAG_OPERATOR,)) + ")"
    )
    _find_operators_re = re.compile(".+" + _comp_operators_re + ".+")
    _requirement_splitter = re.compile(
        "^"
        + r"((?P<index_url>[\w:@/\\?=+^&_\-%$]+)\s*::\s*)?"
        + r"(?P<asset_name>[\w/\-_\d\.]+)"
        + "("
        + _comp_operators_re
        + r"(?P<version>.+)"
        + ")?"
        + "$"
    )

    @classmethod
    def build(
        cls,
        asset_name: str,
        operator: str,
        version: str,
        index_url: str | None = None,
    ):
        idx = ""
        if index_url:
            idx = f"{index_url} "
        return cls(f"{idx}{asset_name}{operator}{version}")

    @classmethod
    def build_version(cls, asset_name: str, version: int | str, index_url: str | None):
        return cls.build(asset_name, cls.CONCREAT_OPERATOR, version, index_url)

    @classmethod
    def build_version_edit(
        cls, asset_name: str, version: int | str, index_url: str | None
    ):
        return cls.build(
            asset_name,
            cls.CONCREAT_OPERATOR,
            str(version) + cls.EDIT_SUFFIX,
            index_url,
        )

    @classmethod
    def build_flag(cls, asset_name: str, flag: str, index_url: str | None = None):
        return cls.build(asset_name, cls.FLAG_OPERATOR, flag, index_url)

    class AssetNotFoundError(ValueError):
        def __init__(self, requirement) -> None:
            super().__init__(
                f"Could not find asset {requirement.asset_name} for {requirement=}"
            )
            self.requirement = requirement

    class FlagNotFoundError(ValueError):
        def __init__(self, requirement) -> None:
            super().__init__(
                f"Could not find flag {requirement.version} for {requirement=}"
            )
            self.requirement = requirement

    class VersionNotFoundError(ValueError):
        def __init__(self, requirement) -> None:
            super().__init__(
                f"Could not find version {requirement.version} for {requirement=}"
            )
            self.requirement = requirement

    class ForbiddenWithEditError(ValueError):
        def __init__(self, requirement, message) -> None:
            super().__init__(f"{message}. The requirement has edit: {requirement}")

    def _cut(self) -> None:
        as_str = str(self)  # back to str
        match = self._requirement_splitter.match(as_str)
        if match is None:
            raise ValueError(f'Invalid requirement "{self}".')
        self._parts = match.groupdict()

    @property
    def parts(self) -> dict[str, str]:
        if not hasattr(self, "_parts"):
            self._cut()
        return self._parts

    @property
    def index_url(self) -> str:
        return self.parts["index_url"]

    @property
    def asset_name(self) -> str:
        return self.parts["asset_name"]

    @property
    def operator(self) -> str:
        return self.parts["operator"]

    @property
    def version(self) -> str:
        return self.parts["version"]

    @property
    def editless_version(self) -> str:
        """
        The version without the edit suffix (if
        there's one)
        See also `is_edit()`
        """
        if not self.has_edit:
            return self.version
        version = self.version
        return version[: -len(self.EDIT_SUFFIX)]

    @property
    def flag(self) -> str:
        if self.has_flag:
            return self.version
        return None

    @property
    def editless_flag(self) -> str:
        if self.has_flag:
            return self.editless_version
        return None

    @property
    def pattern(self) -> str:
        if self.is_abstract:
            return self.parts["version"]
        return None

    @property
    def has_name_only(self) -> bool:
        return self.operator is None

    @property
    def has_edit(self) -> bool:
        """Does the requirement version has edit suffix ?"""
        return (self.version and self.version.endswith(self.EDIT_SUFFIX)) or (
            self.flag and self.flag.endswith(self.EDIT_SUFFIX)
        )

    @property
    def has_flag(self) -> bool:
        return self.operator == self.FLAG_OPERATOR

    @property
    def is_concrete(self) -> bool:
        return self.operator == self.CONCREAT_OPERATOR

    def to(
        self,
        asset_name: Union[str, type[Ellipsis]] = ...,
        operator: Union[str, None, type[Ellipsis]] = ...,
        version_or_flag: Union[int, str, type[Ellipsis]] = ...,
        index_url: Union[str, None, type[Ellipsis]] = ...,
    ):
        if asset_name is ...:
            asset_name = self.asset_name
        if operator is ...:
            operator = self.operator
        if version_or_flag is ...:
            version_or_flag = self.version
        if index_url is ...:
            index_url = self.index_url
        return self.build(asset_name, operator, version_or_flag, index_url)

    def to_edit(self):
        """Create an similar requirement but with edit."""
        if self.has_edit:
            return Requirement(self)
        return self.to(version_or_flag=(self.version or "") + self.EDIT_SUFFIX)

    @property
    def is_abstract(self):
        return not self.is_concrete

    def raise_asset_not_found(self):
        raise self.AssetNotFoundError(self)

    def raise_flag_not_found(self):
        raise self.FlagNotFoundError(self)

    def raise_version_not_found(self):
        raise self.VersionNotFoundError(self)

    def raise_forbidden_with_edit(self, message: str):
        raise self.ForbiddenWithEditError(self, message)
