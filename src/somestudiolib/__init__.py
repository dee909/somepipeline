"""
    Example library one would develop in a studio which is
    using somepipeline.


    - WARNING -

    This package is not part of somepipeline distribution.
    It is only available in the source repository.

    In order to be able to access it, you must checkout the
    sources and intall the package **IN EDITABLE MODE**.
    This will make everything in the src folder available for
    imports.

    This is a hack.
    This is no commun.
    This is not a bug, pleaes don't report on this.

"""

from .asset_index_plugins import install_asset_index_plugins
from .asset_plugins import install_asset_plugins


def install_all_plugins(plugin_registry):
    install_asset_index_plugins(plugin_registry)
    install_asset_plugins(plugin_registry)
