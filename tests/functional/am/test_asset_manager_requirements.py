from somepipeline.am.asset_manager.requirement import Requirement


def test_requirement_cut():
    index_url = "http://user:password@asset_index/index_name?param=value&bool=true"
    asset_name = "type/asset_name"
    operator = "=="
    version = "1.2.3"

    # test w/o index_url
    req = f"{asset_name}{operator}{version}"
    requirement = Requirement(req)
    assert requirement == Requirement(req)
    assert req == str(requirement)
    assert requirement.index_url is None
    assert requirement.asset_name == asset_name
    assert requirement.operator == operator
    assert requirement.version == version

    # test w/ index_url:
    req = f"{index_url}::{asset_name}{operator}{version}"
    requirement = Requirement(req)
    assert requirement == Requirement(req)
    assert req == str(requirement)
    assert requirement.index_url == index_url
    assert requirement.asset_name == asset_name
    assert requirement.operator == operator
    assert requirement.version == version

    # test equality
    assert Requirement(req)


def test_requirement_cuts():
    index_urls = (
        None,
        "index_alias",
        "lan://asset_index",
        "lan://asset_index:8080",
        "lan://asset_index/my_project",
        "lan://asset_index:8080/my_project",
        "s3://asset_index/index_name",
        "http://user:password@asset_index:80/index_name?param=value&bool=true",
        ":@/\\?=+^&_-%$",  # index_url allows those characters
    )
    asset_names = (
        "CHARS/Alice/Alice.ma",
        "CHARS/Alice02/Alice02-anim.ma",
        "CHARS/Alice_old/Chars_Alice_old-anim_cache.abc",
        "CHARS/Alice_old02/bob.ma",
        "CHARS/Alice-old/Alice-old_takavoir.data",
        "CHARS/Alice-old02/Alice-old02-anim_export.editMa",
        "SETS/Kitchen/Kitchen_mod.obj",
        "PROPS/Kitchen/Chair/Kitchen_Chair-lookdev.ma",
        "PROPS/Kitchen/Glass/PROPS_Kitchen_Glass-actor.ma",
        "EP001/PROPS/Kitchen/Glass",
        "EP001/SQ001/PROPS/Kitchen/Glass",
        (
            # flake8 line too long -____-
            "EP001/SQ001/SH001/PROPS/Kitchen/Glass/"
            "EP001_SQ001_SH001-PROPS_Kitchen_Glass-cammap.blend"
        ),
    )
    comp_operators = ("==", "~=")
    comp_versions = ("1", "1.2", "1.2.3", "1.2.3.rc4", "1.2.3+edit")
    flag_operators = ("@",)
    flag_versions = (
        "Blessed",
        "Head",
        "Custom Flag_Name",
        "&é\"'(-è_çà)+~#{}[]|`\\/^¨$*",  # flag allows those characters
    )

    requirements = []

    def add(req, index_url, asset_name, operator, version):
        requirements.append(
            (
                req,
                index_url,
                asset_name,
                operator,
                version,
            )
        )

    for index_url in index_urls:
        idx = ""
        if index_url:
            idx = index_url + "::"
        for asset_name in asset_names:
            # not op / version means @Blessed
            add(f"{idx}{asset_name}", index_url, asset_name, None, None)
            for operator in comp_operators:
                # each comp operator with each version:
                for version in comp_versions:
                    add(
                        f"{idx}{asset_name}{operator}{version}",
                        index_url,
                        asset_name,
                        operator,
                        version,
                    )
            for operator in flag_operators:
                # each flag operator with each flag:
                for version in flag_versions:
                    add(
                        f"{idx}{asset_name}{operator}{version}",
                        index_url,
                        asset_name,
                        operator,
                        version,
                    )

    import logging

    for req, index_url, asset_name, operator, version in requirements:
        logging.warning(req)
        logging.warning((index_url, asset_name, operator, version))

        requirement = Requirement(req)
        assert requirement.index_url == index_url
        assert requirement.asset_name == asset_name
        assert requirement.operator == operator
        assert requirement.version == version
