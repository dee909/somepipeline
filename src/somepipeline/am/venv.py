from pathlib import Path
import logging
import subprocess
import platform

logger = logging.getLogger(__name__)


class _Venv:
    @classmethod
    def create(cls, python: str, parent_path: Path, venv_name: str):
        cmd = [python, "-m", "venv", parent_path / venv_name]
        logger.info(f"Creating venv. {cmd=}")
        subprocess.check_call(cmd)
        logger.info("Venv created.")

    def __init__(self, path: Path) -> None:
        self._path = path

    @property
    def python(self):
        raise NotImplementedError()


class WinVenv(_Venv):
    @property
    def python(self):
        return self._path / "Scripts" / "python.exe"


if platform.system() == "Linux":
    raise RuntimeError(f'system "{platform.system}" not supported yet.')
elif platform.system() == "Windows":
    Venv = WinVenv
elif platform.system() == "Java":
    raise RuntimeError(f'system "{platform.system}" not supported yet.')
