from somepipeline.am.asset_manager.asset_index.asset_index_plugin import AssetIndex


def asset_index_create_versions(
    index: AssetIndex, asset_name: str, version: str, flags: set[str], *dependencies
):
    index.ingest_content("blah...", asset_name, version, "testing stuffs.")
    for flag in flags:
        index.flag_version(asset_name, version, flag)
    if dependencies:
        index.add_version_requirements(asset_name, version, dependencies)
