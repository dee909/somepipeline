import logging
import time

import pytest

from somepipeline.plugins import PluginRegistry, Plugin

from . import utils


def test_plugin_candidature_election_and_override():  # noqa
    """
    Testing / Demonstrating:
        - Multiple candidat
        - Election for single candidat
        - Election for multiple candidat
        - Election result is cached
        - Overridding a plugin election without importing/inheriting
        the original winner.

    """

    class Food(Plugin):
        """
        This is a Plugin category.
        """

        @classmethod
        def plugin_category(cls):
            """Plugin category must implement this."""
            return "Food"

        @classmethod
        def is_plugin_candidat(cls, course: str):
            """Overridden to set the signature."""
            super().is_plugin_candidat(course)

        @classmethod
        def plugin_election(cls, candidats: set[Plugin], course: str):
            """Overridden to set the signature."""
            super().plugin_election(candidats, course)

    class Salad(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            return course in ("starter", "main")

    class Burger(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            return course in ("main",)

        @classmethod
        def plugin_election(cls, candidats: set[Plugin], course: str):
            """The Burger always wins when candidating."""
            if cls in candidats:
                candidats.clear()
                candidats.add(cls)

    class Spaghetti(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            return course in ("main",)

    class Camembert(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            return course in ("entry", "main", "desert", "other")

    class IceCream(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            # Simulate a complicated situation, to test election caching:
            time.sleep(0.01)
            return course in ("desert",)

    registry = PluginRegistry()
    registry.add_plugins(Salad, Burger, Spaghetti, Camembert, IceCream)

    # Assert set of candidat for each course:
    assert registry.get_candidat_plugins("Food", course="starter") == set([Salad])
    assert registry.get_candidat_plugins("Food", course="main") == set(
        [Salad, Burger, Spaghetti, Camembert]
    )
    assert registry.get_candidat_plugins("Food", course="desert") == set(
        [IceCream, Camembert]
    )
    assert registry.get_candidat_plugins("Food", course="other") == set([Camembert])

    # Assert election is cached:
    def time_election():
        t = time.time()
        registry.elect_plugin("Food", "main")
        elapsed = time.time() - t
        print(f"TIMED ELECTION: {elapsed} {registry._cached_elect_plugin.cache_info()}")
        return elapsed

    first_time_tobeaucoup = time_election()
    second_time_bienmieux = time_election()
    assert second_time_bienmieux < first_time_tobeaucoup

    # Assert the Burger wins for main course:
    assert registry.elect_plugin("Food", "main") == Burger

    # Now we want to override the Burger with our CheeseBurger
    # Without having to inherit it !

    class CheeseBurger(Food):
        @classmethod
        def is_plugin_candidat(cls, course: str):
            return course in ("main",)

        @classmethod
        def plugin_election(cls, candidats: set[Plugin], course: str):
            # Replace Burger by me if it's the winning candidat:
            if [p.plugin_type_name() for p in candidats] == ["Burger"]:
                candidats.clear()
                candidats.add(cls)
            return

    registry.add_plugin(CheeseBurger)

    # Assert Cheeseburger is a candidat:
    assert registry.get_candidat_plugins("Food", course="main") == set(
        [Salad, Burger, CheeseBurger, Spaghetti, Camembert]
    )
    # Assert Cheeseburger is elected:
    assert registry.elect_plugin("Food", course="main") == CheeseBurger
