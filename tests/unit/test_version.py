from somepipeline.am.asset_manager.version import Version

import logging


def value_to_fields():
    value_to_fields = [
        (1, (1, 0, 0, False)),
        ("1+edit", (1, 0, 0, True)),
        (2, (2, 0, 0, False)),
        ("2+edit", (2, 0, 0, True)),
        (3, (3, 0, 0, False)),
        ("3+edit", (3, 0, 0, True)),
        ("1.1", (1, 1, 0, False)),
        ("1.1+edit", (1, 1, 0, True)),
        ("1.2", (1, 2, 0, False)),
        ("1.2+edit", (1, 2, 0, True)),
        ("1.3", (1, 3, 0, False)),
        ("1.3+edit", (1, 3, 0, True)),
        ("1.3.1", (1, 3, 1, False)),
        ("1.3.1+edit", (1, 3, 1, True)),
        ("1.3.2", (1, 3, 2, False)),
        ("1.3.2+edit", (1, 3, 2, True)),
        ("1.3.3", (1, 3, 3, False)),
        ("1.3.3+edit", (1, 3, 3, True)),
        ("2", (2, 0, 0, False)),
        ("2+edit", (2, 0, 0, True)),
    ]
    return value_to_fields


def test_version_fields():
    import packaging.version

    assert (
        packaging.version.Version("2.0.0").release
        > packaging.version.Version("1.0.0").release
    )
    assert (
        packaging.version.Version("2.0.0").release
        < packaging.version.Version("3.0.0").release
    )
    for value, (major, minor, micro, has_edit) in value_to_fields():
        logging.warning(
            (
                value,
                (major, minor, micro, has_edit),
            )
        )
        version = Version(value)
        assert version.original == value
        assert version.major == major
        assert version.minor == minor
        assert version.micro == micro
        assert version.has_edit == has_edit

        bumped = version.bump(major=True)
        assert bumped.major == major + 1
        assert bumped.minor == minor
        assert bumped.micro == micro
        assert bumped.has_edit == False
        logging.warning(("?????", version, bumped))
        assert bumped > version
        assert version < bumped

        bumped = version.bump(minor=True)
        assert bumped.major == major
        assert bumped.minor == minor + 1
        assert bumped.micro == micro
        assert bumped.has_edit == False
        assert bumped > version
        assert version < bumped

        bumped = version.bump(micro=True)
        assert bumped.major == major
        assert bumped.minor == minor
        assert bumped.micro == micro + 1
        assert bumped.has_edit == False
        assert bumped > version
        assert version < bumped

        assert version.editless.major == major
        assert version.editless.minor == minor
        assert version.editless.micro == micro
        assert version.editless.has_edit == False
        assert bumped > version
        assert version < bumped
