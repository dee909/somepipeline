from somepipeline.plugins import PluginRegistry

from .mongo_asset_index import MongoAssetIndex


def install_asset_index_plugins(plugin_registry: PluginRegistry):
    plugin_registry.add_plugins(
        MongoAssetIndex,
    )
