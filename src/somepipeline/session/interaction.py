from typing import TYPE_CHECKING


from ..plugins import Plugin, abstractmethod

if TYPE_CHECKING:
    from .session import Session


class InteractionPLugin(Plugin):
    @classmethod
    def plugin_category(cls):
        return "Interaction"

    @abstractmethod
    def set_progress(
        self,
        progress_id: str,
        at_step: str,
        steps: list[str],
        parent_id: str | None = None,
    ):
        ...

    @abstractmethod
    def notify(self, level: int, title: str, message: str):
        ...

    @abstractmethod
    def request_input(self, ui: dict):
        ...

    def __init__(self, session):  #: Session):
        self._session = session

    @property
    def session_name(self):
        return self._session.session_name
