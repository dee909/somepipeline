from .http import HTTPAssetIndex
from .lan import LANAssetIndex
from .memory import MemoryAssetIndex
from .s3 import S3AssetIndex


def install_asset_index_plugins(plugin_registry):
    plugin_registry.add_plugins(
        LANAssetIndex, MemoryAssetIndex, HTTPAssetIndex, S3AssetIndex
    )
