from ..plugins import PluginRegistry

from .default_interaction import DefaultInteraction


class Session:
    _SESSIONS = {}

    @classmethod
    def get(cls, session_name=None, create=True):
        try:
            return cls._SESSIONS[session_name]
        except KeyError:
            if not create:
                raise ValueError(f"Unknown session name {session_name!r}.")
            session = cls(session_name)
            cls._SESSIONS[session_name] = session
            return session

    def __init__(self, name):
        self._name = name
        self._plugin_registry = PluginRegistry()
        self._plugin_registry.add_plugin(DefaultInteraction)

        # TODO: we might need to delegatet this to te creator, if some
        # setup is needed before the plugin registry intall everything.
        self.startup()

    def startup(self):
        self._plugin_registry.install_all()

        interaction_plugin = self.plugin_registry.elect_plugin(
            "Interaction", session=self
        )
        self._interaction = interaction_plugin(self)

    @property
    def session_name(self):
        return self._name

    @property
    def plugin_registry(self):
        return self._plugin_registry

    def set_progress(
        self,
        progress_id: str,
        at_step: str,
        steps: tuple[str],
        parent_id: str | None = None,
    ):
        """
        Shows that progress `progress_id` is now at `step` from `steps`.
        """
        self._interaction.set_progress(progress_id, at_step, steps, parent_id)

    def notify(self, level: str, title: str, message: str):
        self._interaction.notify(level, title, message)

    def request_input(self, ui: dict) -> object | None:
        result = self._interaction.show_ui(ui)
        return result
