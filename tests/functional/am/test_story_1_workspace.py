import logging

import pytest

from somepipeline.am.workspace import Workspace, WorkspaceSettings, Requirement

from . import plugin_utils


def test_create_workspace_and_install_assets(tmp_path):
    """ """
    # This install dummy asset plugin for maya scene
    # and maya work scene. They rely on the asset extension
    # being `.ma` or `.mb`
    # We will need them for this demo, but you don't need
    # to know about it for now :)
    plugin_utils.install_maya_demo_asset_plugins()

    # Create a workspace:
    asset_store_path = tmp_path / "ASSET_STORE"
    asset_store_path.mkdir()

    workspace_path = tmp_path / "Work"
    settings = WorkspaceSettings()
    settings.asset_manager.default_index.url = f"lan://{asset_store_path}"
    workspace = Workspace.create(workspace_path, settings=settings, create_venv=False)

    # The workspace is usable as is, you can install assets etc...
    # but when the worspace already exists, you can simply use:
    workspace = Workspace(workspace_path)
    # and the settings will be loaded from the filesystem.

    # You can install assets to your workspace using the asset manager.
    # This is kind of the main goal actually ^^.
    asset_manager = workspace.asset_manager
    print(asset_manager)

    # An asset is installed using a requirement string:
    #   <asset_name>(==|~=|@)<version_or_flag>
    # For example:
    #   chars/bob-rig@Head       : installs the head version of bob's rig
    #   props/glass-lookdev~=2.* : installs the highest version 2 of the glass lookdev.
    #   sq001/sh001/anim/sq001_sh001-anim_bg.blend : installs the version 3.2.1 of this
    #                                  shot's bg animation blender project
    # (see `somepipeline.am.asset_manager.requirement`)

    # Assets are installed from an asset index. Your workspace
    # settings defines a default index (see at the begining of this
    # function).
    # But this index currently has no asset, so there's nothing you
    # can install for now.
    # Let's confirm that by trying anyway :)
    table_mod = "PROPS/Kitchen/Table/KitchenTable-mod.ma"
    table_lookdev = "PROPS/Kitchen/Table/KitchenTable-lookdev.ma"
    table_rig = "PROPS/Kitchen/Table/KitchenTable-rig.ma"

    # When you ask for an asset, you do it using a `Requirement`.
    # It is a string with the asset name, an operator, and version or
    # flag specifier.
    # The higher version of an asset is always flaged as 'Head'.
    # You require a flag by using the `@` operator, like this:
    table_mod_require = table_mod + "@Head"
    # You can also use the build helper in the Requirement class:
    table_lookdev_require = Requirement.build_flag(table_lookdev, "Head")

    with pytest.raises(Requirement.AssetNotFoundError):
        workspace.install_assets(table_mod_require, table_lookdev_require)
    # See ? this asset doesn't exist, we got an AssetNotFoundError.

    # Let's create them all !

    # When creating the first version of an asset, you often actually import
    # data from somewhere. Let's say our mod file exists outside of the project
    # and we want to import it as an asset.
    # First, we create a fake mod file somewhere:
    external_files = tmp_path / "foreign_files"
    external_files.mkdir()
    external_mod_scene = external_files / "kitchen_table_from_pixar.ma"
    with open(external_mod_scene, "w") as fp:
        fp.write("This is the `mod` content for V1.")

    # Then we use the workspace's asset_manager to ingest this file,
    # turning it into a versioned asset:
    workspace.asset_manager.ingest_asset(
        external_mod_scene, table_mod, "1.0", "Initial commit."
    )

    # Sometimes your first version of an asset is build out of a template
    # or thin air.
    # In this situation, we can use the asset manager to initialize an
    # asset with a given content.
    # Let's create our lookdev scene this way:
    content = "\n# This is some awesome lookdev man!\n"
    workspace.asset_manager.ingest_content(
        content, table_lookdev, "1.0", "Imported from another dimension."
    )

    # Now we can install the mod and the lookdev:
    workspace.install_assets(
        table_mod_require,
        table_lookdev_require,
    )

    # An other situation is when you want to create an asset without
    # publishing anything until you've finished working on it. In this
    # case, you can ask the workspace to import something (a template, or
    # a generated file, ...) as a new asset to later publish:
    external_rig_scene = external_files / "kitchen_table_rig_by_mitch.ma"
    with open(external_rig_scene, "w") as fp:
        fp.write("This is the `rig` template content.")
    workspace.import_asset(external_rig_scene, table_rig)

    # The workspace remembers our requirements and the currently intalled
    # version for each asset.
    # You can see the workspace requirement with `get_required_assets()`.
    # It will return a list containing all the requirement used to install
    # assets:
    requires = workspace.get_required_assets()
    assert set(requires) == set(
        [
            table_mod_require,
            table_lookdev_require,
            # note that table_rig is not listed there since it
            # wasn't installed but rather created from outside data.
        ]
    )
    # And you can see the currently installed assets with `get_installed_assets()`.
    # It will return a list containing a concrete requirement for each installed asset.
    # A concrete requirement is a requirement which point to a specific version rather
    # than a pattern or a flag.
    installed = workspace.get_installed_assets()
    assert set(installed) == set(
        [
            f"{table_mod}==1.0",
            f"{table_lookdev}==1.0",
            # NB: you can use `Requirement.build_edit()` to create this string:
            f"{table_rig}==0+edit",
        ]
    )

    # We required the Head for mod and lookdev, so our copies will be
    # late as soon as someone else creates a new version of them.
    # Let's simulate this on the lookdev by ingesting another version:
    content = "\n# This is some even better lookdev man!\n"
    workspace.asset_manager.ingest_content(
        content, table_lookdev, "1.5", "I've made this way better."
    )

    # damned, we're late on the lookdev !
    # let's update our asset to match our requirements:
    workspace.update_assets()
    # Phewww... feeling better.
    # All asset requirements have been evaluated again and new versions of the
    # assets have been installed when appropriate.

    # We can inspect which version of each asset is now installed:
    installed = workspace.get_installed_assets()
    assert set(installed) == set(
        [
            f"{table_mod}==1.0",
            # NB: this next one has been bumped to 1.5 by `workpace.update_assets()`:
            f"{table_lookdev}==1.5",
            f"{table_rig}==0+edit",
        ]
    )
