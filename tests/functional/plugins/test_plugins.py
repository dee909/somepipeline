import logging

import pytest

from somepipeline.plugins import PluginRegistry, Plugin

from . import utils


def test_plugin_blacklisting_func():
    registry = PluginRegistry()

    def even_letter_count_blacklister(plugin: Plugin) -> bool:
        """
        This blacklister will refuse plugin with a type name
        having an even number of characters.
        """
        if len(plugin.plugin_type_name()) % 2:
            return True

    registry.add_blacklist(even_letter_count_blacklister)

    utils.add_mockup_plugins(
        registry,
        Food=["Salad", "Burger", "Salt"],
        Drink=["Water", "Soda", "Beer"],
    )

    assert set([p.plugin_type_name() for p in registry.get_plugins("Food")]) == set(
        ["Burger", "Salt"]
    )
    assert set([p.plugin_type_name() for p in registry.get_plugins("Drink")]) == set(
        ["Soda", "Beer"]
    )


def test_plugin_blacklisting_class():
    registry = PluginRegistry()

    class EvenLetterCountBlackLister:
        def __init__(self):
            self.blacklisted = set()

        def __call__(self, plugin: Plugin):
            if len(plugin.plugin_type_name()) % 2:
                self.blacklisted.add(plugin)
                return True

    blacklister = EvenLetterCountBlackLister()
    registry.add_blacklist(blacklister)

    utils.add_mockup_plugins(
        registry,
        Food=["Salad", "Burger", "Salt"],
        Drink=["Water", "Soda", "Beer"],
    )

    assert set([p.plugin_type_name() for p in blacklister.blacklisted]) == set(
        ["Salad", "Water"]
    )


def test_plugin_definition_add_get():
    class SuperPlugin(Plugin):
        @classmethod
        def plugin_category(cls):
            return "SuperPlugin"

    class SuperPluginA(SuperPlugin):
        @classmethod
        def is_plugin_candidat(cls, value: int):
            return value % 2

    class SuperPluginB(SuperPlugin):
        @classmethod
        def is_plugin_candidat(cls, value: int):
            return not value % 2

    plugin_registry = PluginRegistry()
    plugin_registry.add_plugins(SuperPluginA, SuperPluginB)

    assert set(plugin_registry.get_plugins(SuperPlugin.plugin_category())) == set(
        (
            SuperPluginA,
            SuperPluginB,
        )
    )


def test_plugin_category():
    """
    Define the plugin category in a higher class.
    """

    class MyStuffPlugin(Plugin):
        @classmethod
        def plugin_category(cls):
            return "Stuff"

    class MyStuffA(MyStuffPlugin):
        pass

    class MyStuffB(MyStuffPlugin):
        pass

    assert MyStuffA.plugin_category() == "Stuff"
    assert MyStuffB.plugin_category() == "Stuff"


def test_plugin_type_name():
    """
    Default plugin type name is the class name.
    Override it my overridding the `plugin_type_name()` classmethod.
    """

    class MyPlugin(Plugin):
        pass

    assert MyPlugin.plugin_type_name() == "MyPlugin"

    class MyStuffPlugin(Plugin):
        @classmethod
        def plugin_type_name(cls):
            return "MyStuff"

    assert MyStuffPlugin.plugin_type_name() == "MyStuff"


def test_is_plugin_candidat():
    """
    I had trouble when mistyping `plugin_category()` and
    the error message was not helping.
    This tests we throw a better error with a descriptive
    message.

    (honestly, my original issue is about abstract classmethods,
    but let's not care ^_^)
    """
    import inspect

    class SuperPlugin(Plugin):
        @classmethod
        def plugin_category(cls):
            return "SuperPlugin"

        @classmethod
        def is_plugin_candidat(cls, value: int):
            """
            Set the signature subclasses must implement.
            """
            # Call the base class to raise a nice exception:
            super().is_plugin_candidat(value)

    class SuperPluginA(SuperPlugin):
        @classmethod
        def is_plugin_candidat(cls, value: int):
            return value % 2

    class SuperPluginB(SuperPlugin):
        @classmethod
        def is_plugin_candidat(cls, value: int):
            return not value % 2

    class SuperPluginC(SuperPlugin):
        "This class forgets to implement `pluggin_supports` !"
        ...

    assert SuperPluginA.is_plugin_candidat(5)
    assert not SuperPluginB.is_plugin_candidat(5)

    # Calling plugin support must raise `NotImplementedError`:
    with pytest.raises(NotImplementedError):
        SuperPluginC.is_plugin_candidat(5)

    # The raised error is actually a `NotImplementedMethodError`
    # which provide more informations:
    #   cls, method_name, method
    # You don't need to except `NotImplementedMethodError` though,
    # excepting a classic `NotImplementedError` will still provide
    # a `NotImplementedMethodErro`
    try:
        SuperPluginC.is_plugin_candidat(5)
    except NotImplementedError as err:
        assert err.cls == SuperPluginC
        assert err.method_name == "is_plugin_candidat"
        assert err.method == SuperPluginC.is_plugin_candidat
