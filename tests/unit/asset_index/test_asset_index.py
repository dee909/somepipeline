from pathlib import Path
from collections import defaultdict
import logging
import stat

import pytest

from somepipeline.am.asset_manager.asset_index.asset_index_plugin import AssetIndex

# A parameterized fixture with all AssetIndex plugins to test:
from .asset_index_fixtures import (
    asset_index,  # fixture, astually used.
    empty_asset_index,  # fixture, actually used.
    get_test_assets_info,
)


def test_asset_index_category(asset_index):
    # asset_index = get_asset_index(index_url)
    assert asset_index.plugin_category() == "AssetIndex"


def test_asset_index_version_comp(empty_asset_index):
    versions = (
        "1",
        "1.0",
        "1.0.1",
        "1.1",
        "1.1.0",
        # "1.1.1rc1", #TODO: make it work !
        # "1.1.1rc2", #TODO: make it work !
        "1.1.1",
        # "1.1.1+edit", #TODO: make it work !
        "1.1.2",
        "1.2",
        "1.2.1",
        "1.3",
        "2",
    )

    sorted = AssetIndex.sorted_versions(versions)
    assert sorted == list(versions)


def test_head_updated_after_ingest(asset_index: AssetIndex):
    # At the time I'm writting this, we're only testing ingest_content()
    # and not ingest_asset() since the index is only populated using inget_content().
    # We should do our own ingests here for the test to be easier
    # to maintain / understand.
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        assert asset_index.get_flag_version(
            asset_name, asset_index.DYNAMIC_FLAGS.HEAD
        ) == str(versions[-1])


def test_get_flags(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        expected_flags = set(flags.keys())
        expected_flags |= {AssetIndex.DYNAMIC_FLAGS.HEAD}
        assert asset_index.get_flags(asset_name) == expected_flags


def test_get_flag_version(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        for flag, version in flags.items():
            assert asset_index.get_flag_version(asset_name, flag) == str(version)

    assert asset_index.get_flag_version(
        asset_name, asset_index.DYNAMIC_FLAGS.HEAD
    ) == str(versions[-1])


def test_get_version_flags(asset_index: AssetIndex):
    def get_version_flags(flags):
        """utility to get a dict version->flag from flag->version."""
        ret = defaultdict(set)
        for flag, version in flags.items():
            ret[version].add(flag)
        return ret

    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        expected_version_flags = get_version_flags(flags)
        for version in versions:
            if version == versions[-1]:
                expected_version_flags[version].add(AssetIndex.DYNAMIC_FLAGS.HEAD)
            assert (
                asset_index.get_version_flags(asset_name, version)
                == expected_version_flags[version]
            )


def test_get_asset_versions(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        str_versions = [str(v) for v in versions]
        assert asset_index.get_asset_versions(asset_name) == str_versions


def test_get_asset_metadata(asset_index: AssetIndex):
    asset_infos = get_test_assets_info()
    for asset_name, asset_type, tags, versions, flags in asset_infos:
        md = asset_index.get_asset_metadata(asset_name)
        assert md.asset_name == asset_name
        assert md.asset_type == asset_type
        assert md.tags == set(tags)
        assert md.versions == [str(v) for v in versions]
        assert len(md.log) > 1  # I know -____-'


def test_set_asset_type(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        asset_metadata = asset_index.get_asset_metadata(asset_name)
        assert asset_metadata.asset_type == asset_type

        # Testing log updates when changing asset_type
        new_asset_type = "My Asset, my rules !"
        asset_index.set_asset_type(asset_name, new_asset_type)
        asset_metadata = asset_index.get_asset_metadata(asset_name)
        # new asset_type is correctly returned in metadata:
        assert asset_metadata.asset_type == new_asset_type

        last_log_entry = asset_metadata.log[-1]
        # the log show it's something on asset_type:
        assert "asset_type" in last_log_entry.title
        # the log description contains the old value and the new one:
        assert new_asset_type in last_log_entry.description
        assert asset_type in last_log_entry.description


def test_add_tags(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        md = asset_index.get_asset_metadata(asset_name)
        assert md.tags == set(tags)

    new_tags = {"TestTag", "TestCategory:TestValue"}
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        asset_index.add_tags(asset_name, new_tags)
        md = asset_index.get_asset_metadata(asset_name)
        for tag in new_tags:
            assert tag in md.tags


def test_remove_tags(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        md = asset_index.get_asset_metadata(asset_name)
        assert md.tags == set(tags)

    new_tags = {"TestTag", "TestCategory:TestValue"}
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        asset_index.add_tags(asset_name, new_tags)
        md = asset_index.get_asset_metadata(asset_name)
        for tag in new_tags:
            assert tag in md.tags


def test_get_version_metadata(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        for version in versions:
            md = asset_index.get_version_metadata(asset_name, version)
            assert md.asset_name == asset_name
            assert md.version == str(version)
            assert md.description
            assert md.flags == asset_index.get_version_flags(asset_name, version)


def test_ingest_content(empty_asset_index: AssetIndex):
    raise NotImplementedError


def test_ingest_asset(empty_asset_index: AssetIndex):
    raise NotImplementedError


def test_copy_asset(empty_asset_index: AssetIndex):
    raise NotImplementedError


def test_has_asset(asset_index: AssetIndex):
    for asset_name in [i[0] for i in get_test_assets_info()]:
        assert asset_index.has_asset(asset_name)


def test_has_version(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        for version in versions:
            logging.warning(
                (asset_name, version, asset_index.get_asset_versions(asset_name))
            )
            assert asset_index.has_version(asset_name, version)


def test_flag_version(asset_index: AssetIndex):
    # test pre-set flags:
    last_asset_name = None
    last_version = None
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        last_asset_name = asset_name
        for flag, version in flags.items():
            last_version = version
            assert asset_index.get_flag_version(asset_name, flag) == str(version)

    asset_name = last_asset_name
    version = last_version

    # test flags with special characters:
    test_flags = (
        "MyFlag",
        "Some Flag",
        "my_flag",
        "some-flag",
    )
    for flag in test_flags:
        asset_index.flag_version(asset_name, version, flag)
        assert asset_index.get_flag_version(asset_name, flag) == str(version)

    # test flags with forbidden type / name:
    test_flags = ("1", "1.1.1", "1.2.3", "@name", [1, 2, 3], object())
    for flag in test_flags:
        with pytest.raises(AssetIndex.FlagValueError):
            asset_index.flag_version(asset_name, version, flag)


def test_checkout_asset(asset_index: AssetIndex):
    raise NotImplementedError


def test_checkout_asset_editable(asset_index: AssetIndex):
    raise NotImplementedError


def test_is_checkout_editable(asset_index: AssetIndex):
    raise NotImplementedError


def _create_file_tree(tmp_path: Path):
    root = tmp_path / "many_files"
    root.mkdir()

    created_paths = []

    def create_files_and_dirs(path: Path, max_depth, depth=0):
        for i in range(3):
            filepath = path / f"file_{i}.txt"
            with open(filepath, "w") as fp:
                fp.write("some file content...")
            created_paths.append(filepath)
        for i in range(2):
            folderpath = path / f"folder_{i}"
            folderpath.mkdir()
            created_paths.append(folderpath)
            if depth < max_depth:
                create_files_and_dirs(folderpath, max_depth, depth + 1)

    create_files_and_dirs(root, max_depth=3)
    return root, created_paths


def _is_read_only(path: Path):
    if not path.exists():
        raise ValueError(f"Path does not exists: {path}")
    mode = path.stat().st_mode
    return mode & stat.S_IREAD and not mode & stat.S_IWRITE


def _is_read_write(path: Path):
    if not path.exists():
        raise ValueError(f"Path does not exists: {path}")
    mode = path.stat().st_mode
    return mode & stat.S_IREAD and mode & stat.S_IWRITE


def test_folder_set_read_only_and_read_write(tmp_path: Path):
    root, paths = _create_file_tree(tmp_path)

    AssetIndex._set_read_only(root)

    for path in paths:
        assert _is_read_only(path)
        assert not _is_read_write(path)

    AssetIndex._set_read_write(root)

    for path in paths:
        assert not _is_read_only(path)
        assert _is_read_write(path)


def test_file_set_read_only_and_read_write(tmp_path: Path):
    filepath = tmp_path / "test_file.txt"
    with open(filepath, "w") as fp:
        fp.write("Test content...")

    AssetIndex._set_read_only(filepath)
    assert _is_read_only(filepath)
    assert not _is_read_write(filepath)

    # AssetIndex._set_read_write(filepath)
    # assert _is_read_write(filepath)


def test_set_version_description(asset_index: AssetIndex):
    raise NotImplementedError


def test_set_version_requirements(asset_index: AssetIndex):
    raise NotImplementedError


def test_remove_version_requirements(asset_index: AssetIndex):
    raise NotImplementedError


def test_add_version_requirements(asset_index: AssetIndex):
    raise NotImplementedError


def test_toggling_inherit_dependencies(asset_index: AssetIndex):
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        version = versions[-1]
        # ensure we have dependencies and inherit them is ON:
        dep_req = ["some_asset@Head"]
        asset_index.add_version_requirements(asset_name, version, dep_req)
        md = asset_index.get_version_metadata(asset_name, version)
        assert md.requirements == set(dep_req)
        assert asset_index.get_asset_metadata(asset_name).versions_inherit_requirements

        # lets create a new version:
        new_version = version + 1
        asset_index.ingest_content(
            "tadaaa!", asset_name, new_version, "assert version inherits requirements."
        )
        md = asset_index.get_version_metadata(asset_name, new_version)
        assert md.requirements == set(dep_req)

        # now toggle off the inheritage and create another version:
        asset_index.set_versions_inherit_requirements(asset_name, False)
        assert not asset_index.get_asset_metadata(
            asset_name
        ).versions_inherit_requirements
        third_version = new_version + 1
        asset_index.ingest_content(
            "tadaaa!",
            asset_name,
            third_version,
            "assert version does not inherit requirements.",
        )
        md = asset_index.get_version_metadata(asset_name, third_version)
        assert not md.requirements


def test_checkin_asset(asset_index: AssetIndex, tmp_path):
    new_content = "This is the updated content we need to publish..."
    new_description = "a better version"
    checkin_flags = ("NewFlag", "Blessed")

    checkout_dir = tmp_path / "ChekoutFolder"
    for asset_name, asset_type, tags, versions, flags in get_test_assets_info():
        version = versions[-1]
        assert not asset_index.is_checkout_editable(asset_name, checkout_dir)
        checkout_path = asset_index.checkout_asset(
            asset_name, version, checkout_dir, editable=True
        )
        assert checkout_path.exists()
        assert not checkout_path.is_dir()  # this is only true for our test cases
        assert asset_index.is_checkout_editable(asset_name, checkout_dir)

        with open(checkout_path, "w") as fp:
            fp.write(new_content)

        new_version = asset_index.checkin_asset(
            asset_name,
            checkout_path,
            new_description,
            original_version=version,
            bump_major=True,
            flags=checkin_flags,
        )
        md = asset_index.get_version_metadata(asset_name, new_version)
        assert md.description == new_description
        if 0:
            # Not implemented yet. Care to do it maybe ? ^_^
            assert md.created_from.version == version
            assert md.created_from.bump_major == True
            assert md.created_from.bump_minor == False
            assert md.created_from.bump_micro == False

        for flag in checkin_flags:
            assert asset_index.get_flag_version(asset_name, flag) == new_version
