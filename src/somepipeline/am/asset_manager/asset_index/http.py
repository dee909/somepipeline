import logging
from urllib.parse import urlparse, ParseResult

from .asset_index_plugin import AssetIndex
from ..requirement import Requirement


logger = logging.getLogger(__name__)


class HTTPAssetIndex(AssetIndex):
    """
    Assets stored on a local acessible network.
    """

    @classmethod
    def is_plugin_candidat(cls, url_info: ParseResult):
        return url_info.scheme == "http"
