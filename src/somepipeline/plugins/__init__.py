from __future__ import annotations

from typing import Protocol
from abc import ABC, abstractmethod
import importlib.metadata
import collections
import os
import logging
import inspect
import traceback
from functools import lru_cache

import pydantic


logger = logging.getLogger(__name__)


class NotImplementedMethodError(NotImplementedError):
    @staticmethod
    def get_missing_method_name(cls):
        stack = inspect.stack()
        method_name = stack[2][3]
        return method_name

    def __init__(self, cls, message: str | None = None) -> None:
        self.method_name = self.get_missing_method_name(cls)
        self.cls = cls
        self.method = getattr(cls, self.method_name)
        if message:
            message = f"{message} ({self.method})"
        else:
            message = self.method
        super().__init__(message)


class Plugin(ABC):
    """
    Base for all plugins.

    Subclasses can use @abstractmethod to define
    required interface.
    """

    @classmethod
    def plugin_type_name(cls):
        return cls.__name__

    @classmethod
    @abstractmethod
    def plugin_category(cls):
        """
        Subclasses must implement this
        to specify the plugin category.

        It will most likely return the name of the
        next subclass, but this is not mandatory.
        """
        # absctractmethod doen't apply for class method
        # (or at least, I din't get it to work like I wanted)
        # so:
        raise NotImplementedMethodError(cls)

    @classmethod
    @abstractmethod
    def is_plugin_candidat(cls, *args, **kwargs):
        """
        Depending on the Plugin usage, the right plugin
        may need to be selected out of all plugins in the
        category.

        Subclasses must return True here to be selected
        for the given arguments.
        """
        # absctractmethod doen't apply for class method
        # (or at least, I din't get it to work like I wanted)
        # so:
        raise NotImplementedMethodError(cls)

    @classmethod
    def plugin_election(
        cls,
        candidats: set[Plugin],
        *candidats_args,
        **candidats_kwargs,
    ):
        """
        When a single plugin has to be selected for a job,
        the `is_plugin_candidat()` method is not enough: it
        may result in several candidats.

        In this situation, this method is called on all
        candidats, until a single candidat remains in the
        set of candidats.

        The election ends when no plugin has altered the
        set of candidats during the last election round.
        Exception are raise if the set of candidat does not
        contains exaclty one Plugin then.

        Default implementation does nothing.

        Subclass may override this and alter the set of
        candidats: add plugins, remove plugins, clear the set, ...
        """
        return


class PluginRegistrySettings(pydantic.BaseSettings):
    PLUGIN_INSTALLER_PATH: str = ""
    PLUGIN_BLACKLIST: str = ""

    class Config:
        env_prefix = "SPL_"


class Installer(Protocol):
    def __call__(self, plugin_registry: PluginRegistry) -> None:
        """
        An installer is a callable taking a `plugin_registry` argument.
        It is used to call:
            `plugin_registry.add_plugin(plugin_type, some_plugin)`
        to make `some_plugin` available to modules using plugin registered
        as `plugin_type`.
        """
        ...


class BlackLister(Protocol):
    def __call__(self, plugin: Plugin) -> bool:
        """
        A BlacLister is a callable which return True if the
        given plugin shouldn't be used.
        """
        ...


class PluginTypeNameBlackLister:
    """
    This BlackLister will block plugin with `plugin_type_name()`
    found in the constructor's `blacklisted_plugin_names` argument.
    """

    def __init__(self, blacklisted_plugin_names):
        self._blacklisted_plugin_names = blacklisted_plugin_names

    def __call__(self, plugin: Plugin) -> bool:
        return plugin.plugin_type_name() in self._blacklisted_plugin_names


PLUGIN_ENTRY_POINT_NAME = "somepipeline.plugins"


class PluginRegistry:
    f"""
    The plugin registry holds plugin types by category.

    Plugins can be registered with `add_plugin(plugin:Plugin)`
    and all plugins of a category can be retrieved with `get_plugins(category:str)`

    A Plugin has `plugin_type_name()` and `plugin_category()` classmethod.

    Plugin can be added by python scripts listed in
    the `SPL_PLUGIN_INSTALLERS` env var.
    Each value in this PATH like env var will be executed as a python script
    with a `plugin_registry` global variable.
    The scripts can use `plugin_registry.add_plugins()` to register custom plugins.

    Plugin can be discovered on the "{PLUGIN_ENTRY_POINT_NAME}" entry points.
    Your packages can return an `Installer` callable which will receive a
    `plugin_registry` argument to let them `add_plugins()`.

    You can blacklist plugin to prevent them from being registered.
    Use `add_blacklist(blacklister: BlackLister)` to add your `BlackLister`
    callable.

    A default blacklister is installed to block plugin which `plugin_type_name()`
    returns a string found in the env var `SPL_PLUGIN_BLACKLIST`.

    You can of course add BlackListers using installer scripts or entry
    point installers.

    """

    def __init__(self, settings=PluginRegistrySettings()) -> None:
        self._blacklisters: list[BlackLister] = []

        # plugin category -> set(Plugin)
        self._plugins: dict[str, set[Plugin]] = collections.defaultdict(set)

        self._plugin_installer_scripts = [
            name.strip()
            for name in settings.PLUGIN_INSTALLER_PATH.split(os.pathsep)
            if name.strip()
        ]

        black_listed_plugin_names = [
            name.strip()
            for name in settings.PLUGIN_BLACKLIST.split(os.pathsep)
            if name.strip()
        ]
        self.add_blacklist(PluginTypeNameBlackLister(black_listed_plugin_names))

    def install_all(self):
        self.install_from_entry_points()
        self.install_from_scripts()

    def install_from_entry_points(self):
        logger.info(f"Installing plugins from entry points {PLUGIN_ENTRY_POINT_NAME!r}")
        entry_points = importlib.metadata.entry_points(group=PLUGIN_ENTRY_POINT_NAME)
        for entry_point in entry_points:
            logger.info(f"  {entry_point}")
            installer = entry_point.load()
            self._run_installer(installer)

    def install_from_scripts(self):
        # TODO: implement this to use call `install_plugins(self)`
        # on each script in `self._script_plugins`.
        logger.warning("Script plugin installation not implemented.")

    def _run_installer(self, installer: Installer):
        try:
            installer(self)
        except Exception as err:
            logger.warning(traceback.format_exc())
            logger.warning(f"Error while running plugin installer {installer}: {err}")

    def add_blacklist(self, blacklister: BlackLister):
        self._blacklisters.append(blacklister)

    def allow_plugin(self, plugin: Plugin):
        for blacklister in self._blacklisters:
            if blacklister(plugin):
                return False
        return True

    def add_plugin(self, plugin: Plugin):
        if self.allow_plugin(plugin):
            self._plugins[plugin.plugin_category()].add(plugin)
        else:
            logger.info(f"Skiping black-listed plugin {plugin}.")

    def add_plugins(self, *plugins: Plugin):
        [self.add_plugin(plugin) for plugin in plugins]

    def get_plugins(self, category: str):
        return tuple(self._plugins[category])

    def plugin_categories(self):
        return sorted(self._plugins.keys())

    def get_candidat_plugins(self, category, *args, **kwargs) -> set[Plugin]:
        """
        Return a list of Plugin of the given category which are
        candidats for the given args/kwargs.
        """
        candidats = []
        for plugin in self.get_plugins(category):
            if plugin.is_plugin_candidat(*args, **kwargs):
                candidats.append(plugin)

        return set(candidats)

    @classmethod
    def _plugin_set_hash(self, plugins: set[Plugin]):
        """
        Returns a (kind of suitable) hash for a set of plugins.
        """
        return hash(tuple(sorted([id(p) for p in plugins])))

    def elect_plugin(self, category: str, *args, **kwargs):
        # We dont cache this method directly because lru_cache
        # fucks up the completion on the decorated method :/
        return self._cached_elect_plugin(category, *args, **kwargs)

    @lru_cache(maxsize=None)
    def _cached_elect_plugin(self, category: str, *args, **kwargs):
        logger = logging.getLogger(f"{__name__}:PluginRegistry.elect_plugin")
        logger.debug(f"ELECTION START {category=}, {args=}, {kwargs=}")

        candidats = self.get_candidat_plugins(category, *args, **kwargs)
        if not candidats:
            raise Exception(
                f"No candidat for {category} Plugin election. "
                f"Candidature args were: {args}, {kwargs} "
            )
        if len(candidats) == 1:
            # It there's only one candidat, he wins the election
            # TODO: Or does he ? maybe he can decide to cancel ?
            # (Let's wait for a case of this to implement resignation.)
            return candidats.pop()

        # keep the original candidats in case of exceptions:
        remaining_candidats = set(candidats)

        # this will be set to false if no plugin altered candidats:
        need_election_round = True

        # let's add little protection:
        max_rounds = 100
        current_round = 0

        # Until no one changed the candidats sets, we'll keep electing:
        while need_election_round:
            current_round += 1
            if current_round > max_rounds:
                raise Exception(
                    f"This {category} Plugin election takes too much time, aborting! "
                    f"Candidature args were: {args}, {kwargs} "
                    f"Candidats were: {candidats}. "
                    f"Remaining candidats at abort time were: {remaining_candidats}"
                )
            need_election_round = False
            for plugin in candidats:
                remaining_candidats_hash = self._plugin_set_hash(remaining_candidats)
                logger.debug(
                    f"--- ELECTING BY {plugin.plugin_type_name()}: "
                    f"{[p.plugin_type_name() for p in remaining_candidats]}"
                )
                plugin.plugin_election(remaining_candidats, *args, *kwargs)
                if remaining_candidats_hash != self._plugin_set_hash(
                    remaining_candidats
                ):
                    logger.debug(
                        f"------ CHANGED BY {plugin.plugin_type_name()}: "
                        f"{[p.plugin_type_name() for p in remaining_candidats]}"
                    )
                    need_election_round = True

        try:
            elected = remaining_candidats.pop()
        except IndexError:
            raise Exception(
                f"No candidat left for {category} Plugin election. "
                f"Candidature args were: {args}, {kwargs} "
                f"Candidats were: {candidats}. "
            )

        if remaining_candidats:
            raise Exception(
                f"Too much candidat left for {category} Plugin election. "
                f"Candidature args were: {args}, {kwargs} "
                f"Candidats were: {candidats}. "
                f"Remaining candidats are: {remaining_candidats}"
            )

        return elected


plugin_registry = PluginRegistry()
