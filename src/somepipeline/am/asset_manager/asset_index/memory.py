from pathlib import Path
import logging
from urllib.parse import urlparse, ParseResult
import os
import shutil
import fnmatch

from .asset_index_plugin import AssetIndex, AssetMetadata, VersionMetadata
from ..requirement import Requirement

logger = logging.getLogger(__name__)


class AssetVersion:
    def __init__(self, asset_name: str, version: str, description: str, content: str):
        self.content = content
        self.metadata = VersionMetadata(
            asset_name=asset_name,
            description=description,
            version=version,
        )


class Asset:
    def __init__(self, name):
        self.name = name
        self._versions = {}
        self._flags = {}

        # metadata:
        self._metadata = AssetMetadata(asset_name=self.name, asset_type="???")

    def get_metadata(self) -> AssetMetadata:
        return self._metadata.copy()

    def has_version(self, version: str):
        return version in self._versions

    def add_version(self, version: str, description: str, content: str):
        log_message_lines = []
        # get head before creating the version:
        # (its not mandatory right now but it might be in the futur.)
        try:
            head_version = self.get_flag(MemoryAssetIndex.DYNAMIC_FLAGS.HEAD)
        except KeyError:
            head_md = None
            log_message_lines.append(
                f"No @Head version found to inherit metadata, using defaults."
            )
        else:
            head = self.get_version(head_version)
            head_md = head.metadata
            log_message_lines.append(
                f"Heriting metadata from @Head (=={head_version})."
            )

        try:
            self._versions[version]
        except KeyError:
            pass
        else:
            raise KeyError(f"Version {version} already exists for asset {self.name}.")
        new_version = AssetVersion(self.name, version, description, content)
        # inherit some metadata from head:
        if head_md is not None:
            if self._metadata.versions_inherit_requirements:
                new_version.metadata.requirements = head_md.requirements.copy()
                log_message_lines.append(
                    f"Herited requirements from @Head (=={head_version})."
                )
            new_version.metadata.custom_data = head_md.custom_data
            log_message_lines.append(
                f"Herited custom data from @Head (=={head_version})."
            )

        MemoryAssetIndex._log_in_metadata(
            new_version.metadata, "New Version", "\n".join(log_message_lines)
        )
        self._versions[version] = new_version

    def get_version(self, version: str) -> AssetVersion:
        try:
            return self._versions[str(version)]
        except KeyError:
            raise KeyError(f"Version {version} does not exists for asset {self.name}.")

    def versions(self):
        return self._versions.keys()

    def set_flag(self, version: str, flag: str):
        if not self.has_version(version):
            raise KeyError(
                f"Version {version} does not exists for asset {self.name}. "
                "Can't flag it."
            )
        self._flags[flag] = version
        MemoryAssetIndex._log_in_metadata(
            self._metadata, "flags", f"flag {flag} set to version {version}."
        )

    def get_flag(self, flag: str):
        try:
            return self._flags[flag]
        except KeyError:
            raise KeyError(f"Flag {flag} not set on asset {self.name}. Can't get it.")


class DB:
    def __init__(self):
        self._assets = {}

    def _get_asset(self, asset_name) -> Asset:
        try:
            asset = self._assets[asset_name]
        except KeyError:
            asset = Asset(asset_name)
            self._assets[asset_name] = asset
        return asset

    def add_version(
        self, asset_name: str, version: str, description: str, content: str
    ):
        asset = self._get_asset(asset_name)
        asset.add_version(version, description, content)

    def has_asset(self, asset_name: str):
        return asset_name in self._assets

    def has_version(self, asset_name: str, version: str):
        return asset_name in self._assets and self._get_asset(asset_name).has_version(
            version
        )

    def get_asset_metadata(self, asset_name):
        return self._get_asset(asset_name).get_metadata()

    def get_versions(self, asset_name: str):
        return self._get_asset(asset_name).versions()

    def get_version(self, asset_name: str, version: str) -> str:
        return self._get_asset(asset_name).get_version(version).content

    def get_version_metadata(
        self, asset_name: str, version: int | str
    ) -> VersionMetadata:
        return self._get_asset(asset_name).get_version(str(version)).metadata

    def set_flag(self, asset_name: str, version: str, flag: str):
        self._get_asset(asset_name).set_flag(version, flag)

    def get_flag(self, asset_name: str, flag: str) -> str:
        return self._get_asset(asset_name).get_flag(flag)


class MemoryAssetIndex(AssetIndex):
    """
    An index with assets stored in memory, usefull for unit/funcion tests.
    """

    @classmethod
    def is_plugin_candidat(cls, url_info: ParseResult):
        return url_info.scheme == "memory"

    def _setup(self, url: str, url_info: ParseResult):
        self._DB = DB()
        self.username = url_info.username

    def get_flags(self, asset_name: str) -> set[str]:
        asset = self._DB._get_asset(asset_name)
        flags = set(asset._flags.keys())
        flags.add(self.DYNAMIC_FLAGS.HEAD)
        return flags

    def get_flag_version(self, asset_name: str, flag: str):
        # flags = self._DB._get_asset(asset_name)._flags
        # logger.warning(f"get_flag_versions {asset_name} {flag}: {flags}")

        # FIXME: we should raise exceptions from the AssetIndex scope,
        # not from Requirement scope :/
        try:
            version = self._DB.get_flag(asset_name, flag)
        except KeyError:
            Requirement.build_flag(asset_name, flag).raise_flag_not_found()
        if not self._DB.has_version(asset_name, str(version)):
            Requirement.build_version(asset_name, version).raise_version_not_found()
        return version

    def get_asset_versions(
        self,
        asset_name: str,
        require_operator: str | None = None,
        require_pattern: str | None = None,
    ):
        # TODO: support exclusion of yanked versions
        versions = self.sorted_versions(self._DB.get_versions(asset_name))

        if require_operator is None:
            return versions

        elif require_operator == "~=":
            versions = [
                version
                for version in versions
                if fnmatch.fnmatch(version, require_pattern)
            ]
            # the pattern X.Y.* does not include X.Y
            # but we should include it (lower bound is >=)
            # so:
            if require_pattern.endswith(".*"):
                lower = require_pattern[:-2]
                if self.has_version(asset_name, lower):
                    versions.insert(0, lower)
            return versions
        else:
            raise ValueError(
                f"Unsupported asset versions filtering: {require_operator=}."
            )

    def get_asset_metadata(self, asset_name: str) -> AssetMetadata:
        md = self._DB.get_asset_metadata(asset_name)
        md.versions = self.get_asset_versions(asset_name)
        return md

    def set_versions_inherit_requirements(self, asset_name: str, b: bool):
        md = self._DB._get_asset(asset_name)._metadata
        before = md.versions_inherit_requirements
        if before == b:
            self._log_in_metadata(
                md, "versions_inherit_requirements", f"Was already {b!r}, as requested."
            )
        else:
            md.versions_inherit_requirements = b
            self._log_in_metadata(
                md, "versions_inherit_requirements", f"changed from {before!r} to {b!r}"
            )

    def set_asset_type(self, asset_name: str, asset_type: str):
        self._DB._get_asset(asset_name)._metadata.asset_type
        md = self._DB._get_asset(asset_name)._metadata
        before = md.asset_type
        md.asset_type = asset_type
        self._log_in_metadata(
            md, "asset_type", f"changed from {before!r} to {asset_type!r}"
        )

    def add_tags(self, asset_name: str, tags: set[str]):
        md = self._DB._get_asset(asset_name)._metadata
        before = md.tags.copy()
        md.tags |= set(tags)
        self._log_in_metadata(
            md, "tags", f"added {tags} = changed from {before!r} to {md.tags!r}"
        )

    def remove_tags(self, asset_name: str, tags: set[str]):
        md = self._DB._get_asset(asset_name)._metadata
        before = md.tags.copy()
        md.tags -= set(tags)
        self._log_in_metadata(
            md, "tags", f"added {tags} = changed from {before!r} to {md.tags!r}"
        )

    def get_version_metadata(self, asset_name: str, version: str) -> VersionMetadata:
        md = self._DB.get_version_metadata(asset_name, version).copy()
        # update flags value which are only stored on assets:
        md.flags = self.get_version_flags(asset_name, version)
        return md

    def set_version_description(self, asset_name: str, version: str, description: str):
        md = self._DB.get_version_metadata(asset_name, version)
        before = md.description
        md.description = description
        self._log_in_metadata(
            md, "description", f"changed from {before!r} to {description!r}"
        )

    def set_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._DB.get_version_metadata(asset_name, version)
        md.requirements = set(requirements)

    def add_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._DB.get_version_metadata(asset_name, version)
        md.requirements |= set(requirements)

    def remove_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        md = self._DB.get_version_metadata(asset_name, version)
        md.requirements -= set(requirements)

    def ingest_content(
        self, content: Path, asset_name: str, version: int | str, description: str
    ):
        self._DB.add_version(asset_name, str(version), description, content)
        self._on_new_version_created(asset_name, version)

    def ingest_asset(
        self, source_path: Path, asset_name: str, version: int | str, description: str
    ):
        with open(source_path, "r") as fp:
            content = fp.read()
        self._DB.add_version(asset_name, str(version), description, content)
        self._on_new_version_created(asset_name, version)

    def copy_asset(self, source: Path, destination: Path):
        """
        Copy anything that could be an asset from source to destination.
        The destination may be adapted for a later checkin.
        """
        destination.parent.mkdir(parents=True, exist_ok=True)
        if source.is_dir():
            shutil.copytree(source, destination)
        else:
            shutil.copy(source, destination)

    def has_asset(self, asset_name: str):
        return self._DB.has_asset(asset_name)

    def has_version(self, asset_name: str, version: int | str):
        return self._DB.has_version(asset_name, str(version))

    def _flag_version(self, asset_name: str, version: int | str, flag: str):
        self._DB.set_flag(asset_name, str(version), flag)

    def checkout_asset(
        self, asset_name: str, version: str, destination: Path, editable: bool
    ) -> Path:
        checkout_path = self.checkout_path(asset_name, destination)
        checkout_path.parent.mkdir(
            parents=True, exist_ok=True
        )  # this exists_ok freaks me out :'(

        with open(checkout_path, "w") as fp:
            fp.write(self._DB.get_version(asset_name, version))

        if not editable:
            self._set_read_only(checkout_path)

        return checkout_path
