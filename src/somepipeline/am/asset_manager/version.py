# from typing import Union
import packaging.version
import logging

logger = logging.getLogger(__name__)


class Version(packaging.version.Version):
    """
    A Version, as used by pip etc..., with support
    for 'edit' property, bumping, original value, etc...

    """

    EDIT_FLAG = "edit"

    def __init__(self, version: int | str) -> None:
        self._original = version
        super().__init__(str(version))

    @property
    def original(self):
        return self._original

    @property
    def has_edit(self):
        return self.local == self.EDIT_FLAG

    @property
    def editless(self):
        if self.has_edit:
            return Version(self.public)
        return self

    def bump(self, major=False, minor=False, micro=False):
        bump_args = (major, minor, micro)
        logger.warning(("BUMPING", self, bump_args))
        if sorted(bump_args) != [False, False, True]:
            raise ValueError(
                f"Cannot bump version {self} with args"
                f"{major=} {minor=} {micro=}:"
                "You must choose only one to bump."
            )

        self.__str__
        temp = Version(self)
        release = list(temp.release)
        # the packaging.version.Version.release.__docstring__ says
        # it indclude trailing zero, but it doesn't :/ (python 3.10.8 here)
        # so:
        while len(release) < 4:
            release.append(0)

        if major:
            release[0] += 1
        if minor:
            release[1] += 1
        if micro:
            release[2] += 1

        # remove trailing zeros:
        while release[-1] == 0:
            release.pop()

        temp._version = packaging.version._Version(
            epoch=temp._version.epoch,
            release=release,
            pre=temp._version.pre,
            post=temp._version.post,
            dev=temp._version.dev,
            # Remove any local field when bumping:
            local=[],
        )

        logger.warning(("BUMPED", temp))
        # now rebuild a clean one, our temp one has been messed up with:
        return Version(temp)
