from typing import Optional
from enum import Enum
from pathlib import Path
import json
import logging
import contextlib
import time
import getpass
import platform
import shutil

import pydantic
import tomlkit

from somepipeline.plugins import plugin_registry
from .workarea import Workarea
from .venv import Venv
from .asset_manager import AssetManager, Requirement
from .asset import Asset
from .asset.asset_operations import OperationReport

logger = logging.getLogger(__name__)


class SettingsAssetIndex(pydantic.BaseModel):
    url: str | None = None


class SettingsAssetIndexAlias(pydantic.BaseModel):
    alias: str
    index: SettingsAssetIndex


class SettingsAssetManager(pydantic.BaseModel):
    # url: pydantic.AnyUrl = "lan://unknown_store"
    default_flag: str = "Blessed"
    default_index: SettingsAssetIndex = SettingsAssetIndex()
    index_aliases: list[SettingsAssetIndexAlias] = []


class SettingsPy(pydantic.BaseModel):
    default_venv: Optional[str] = None
    extra_venvs: list[str] = []
    requirements: list[str] = []


class SettingsWork(pydantic.BaseModel):
    ins: list[str] = []
    outs: list[str] = []


class SettingsWorkareaLookup(str, Enum):
    PARENT_DIR = "PARENT_DIR"
    PARENT_DIRS = "PARENT_DIRS"
    PATH = "PATH"


class SettingsWorkarea(pydantic.BaseModel):
    lookup: SettingsWorkareaLookup = SettingsWorkareaLookup.PARENT_DIR
    path: Optional[Path] = None


class WorkspaceSettings(pydantic.BaseModel):
    workarea: SettingsWorkarea = SettingsWorkarea()
    user: Optional[str] = None

    asset_manager: SettingsAssetManager = SettingsAssetManager()
    asset_requirements: list[Requirement] = []

    py: SettingsPy = SettingsPy()
    works: list[SettingsWork] = []

    class Config:
        use_enum_values = True


class Workspace:
    @classmethod
    def create(cls, path: Path, settings: WorkspaceSettings, create_venv=True):
        workspace = Workspace(path)
        workspace.set_settings(settings)
        workspace.ensure_exists()

        if create_venv:
            workarea = workspace.workarea

            python = workarea.current_python
            maj, min, _patch = workarea.current_python_version
            venv_name = f"venv{maj}{min}"

            workspace.create_venv(python, venv_name, make_default=True)

        workspace.save_settings()
        return workspace

    def __init__(self, path: Path) -> None:
        self._path: Path = path

        # lazy properies:
        self._workarea: Workarea = None
        self._asset_manager: AssetManager = None

        self._settings = WorkspaceSettings()
        if self.settings_filename.exists():
            self.load_settings()

        # Asset instances cache:
        self._asset_instances: dict[str:Asset] = {}

    @property
    def path(self) -> Path:
        return self._path

    @property
    def settings_filename(self) -> Path:
        return self._path / "workspace.json"

    @property
    def assets_path(self) -> Path:
        return self.path / "assets"

    @property
    def py_path(self) -> Path:
        return self.path / "py"

    @property
    def user(self) -> str:
        user = self._settings.user
        if user is None:
            return self.workarea.user

    def _find_workarea(self) -> Workarea:
        if self._settings.workarea.lookup == SettingsWorkareaLookup.PARENT_DIR:
            path = (self._path / "..").resolve()
        elif self._settings.workarea.lookup == SettingsWorkareaLookup.PATH:
            path = self._settings.workarea.path
        elif self._settings.workarea.lookup == SettingsWorkareaLookup.PARENT_DIRS:
            # recursive parent lookup
            raise NotImplementedError(
                f"workarea lookup {SettingsWorkareaLookup.PARENT_DIRS} not implemented."
            )
        else:
            raise ValueError(
                f"Unsupported workarea lookup method: {self._settings.workarea.lookup}"
            )
        return Workarea(path)

    @property
    def workarea(self) -> Workarea:
        if self._workarea is None:
            self._workarea = self._find_workarea()
        return self._workarea

    def _create_asset_manager(self) -> AssetManager:
        am = AssetManager()
        am.set_default_flag(self._settings.asset_manager.default_flag)
        am.set_default_index(self._settings.asset_manager.default_index.url)
        for alias, index_info in self._settings.asset_manager.index_aliases:
            am.set_index_alias(alias, index_info.url)
        return am

    @property
    def asset_manager(self) -> AssetManager:
        if self._asset_manager is None:
            self._asset_manager = self._create_asset_manager()
        return self._asset_manager

    def exists(self):
        return (
            self._path.exists()
            and self.settings_filename.exists()
            and self.assets_path.exists()
            and self.py_path.exists()
        )

    def ensure_exists(self):
        # FIXME: rename this!!!
        # This will fail if the folder already exists
        # Chose if it's `ensure` or `create`...
        # (if it's create, it should fail if
        # setting_filename already exists)
        self._path.mkdir(parents=True)
        self.assets_path.mkdir()
        self.py_path.mkdir()
        if not self.settings_filename.exists():
            self.save_settings()

    def set_settings(self, settings: WorkspaceSettings):
        # TODO: can we really change this anytime w/o messing everything up ?
        self._workarea = None
        self._asset_store = None
        self._settings = settings

    def load_settings(self):
        with open(self.settings_filename, "r") as fp:
            data = json.load(fp)
        self.set_settings(WorkspaceSettings(**data))

    def save_settings(self):
        with open(self.settings_filename, "w") as fp:
            fp.write(self._settings.json())

    def create_venv(self, python: str, venv_name: str, make_default: bool = True):
        Venv.create(python=python, parent_path=self.py_path, venv_name=venv_name)
        if make_default:
            self._settings.py.default_venv = venv_name
            self.save_settings()
        else:
            self._settings.py.extra_venvs.append(venv_name)
        logger.info(f"Default venv updated to {venv_name}.")

    def get_venv(self, venv_name) -> Venv:
        return Venv(self.py_path / venv_name)

    @property
    def python(self, venv_name: Optional[str] = None):
        venv_name = venv_name or self._settings.py.default_venv
        if venv_name is None:
            raise ValueError("No default venv is set, you must provide a venv_name.")
        return self.get_venv(venv_name).python

    @contextlib.contextmanager
    def _install_lock(self):
        lock_file = self.assets_path / "install_lock"
        nb_tries = 0
        while lock_file.exists():
            nb_tries += 1
            time.sleep(0.1)
            logger.debug(f"Waiting for lock_file to be released: {lock_file}.")
            if nb_tries > 10:
                raise RuntimeError(
                    "Could not acquire lock to install assets! Aborting."
                )

        username = getpass.getuser()
        netword_name = platform.node()
        ctime = time.ctime()
        with open(lock_file, "w") as fp:
            fp.write(f"Locked by {username} on {netword_name} at {ctime}\n")
            try:
                yield
            finally:
                fp.close()
                lock_file.unlink()

    def _installed_assets_filename(self):
        return self.assets_path / "installed.txt"

    def get_required_assets(self) -> list[Requirement]:
        return [Requirement(req) for req in self._settings.asset_requirements]

    def get_edit_required_assets(self) -> list[Requirement]:
        """
        Return a list of requirement for all editable assets required.
        """
        return [
            requirement
            for requirement in self.get_required_assets()
            if requirement.has_edit
        ]

    def get_installed_assets(self) -> list[Requirement]:
        """
        Return a Requirement for each installed asset.
        """
        installed_assets_filename = self._installed_assets_filename()
        if not installed_assets_filename.exists():
            return []
        with open(installed_assets_filename, "r") as fp:
            requirements = [Requirement(line.strip()) for line in fp.readlines()]
        return requirements

    def get_edit_installed_assets(self) -> list[Requirement]:
        """
        Return a list of concrete requirement for all asset installed in edit mode.
        """
        return [
            requirement
            for requirement in self.get_installed_assets()
            if requirement.has_edit
        ]

    def get_asset(self, asset_name) -> Asset:
        try:
            return self._asset_instances[asset_name]
        except KeyError:
            # let's instanciate it:
            pass

        # TODO: fetch metadata from checkouted version, and use it to define those:
        asset_type_name = "Asset"
        # NB: this must be hashable for the election cache to work:
        asset_tags = ("tata", "titi", "prod:toto", "entity:ep001_sq001_sh001")

        asset_plugin = plugin_registry.elect_plugin(
            "Asset", asset_name, asset_type_name, asset_tags
        )
        asset = asset_plugin(self, asset_name)
        self._asset_instances[asset_name] = asset
        return asset

    def has_asset(self, asset_name: str):
        return (self.assets_path / asset_name).exists()

    def get_assets(self) -> list[Asset]:
        return [self.get_asset(req.asset_name) for req in self.get_installed_assets()]

    def _save_installed_assets(self, installed: list[Requirement]):
        with open(self._installed_assets_filename(), "w") as fp:
            fp.write("\n".join([str(path) for path in installed]))

    def _forget_installed_assets(self, asset_names: list[str]):
        """
        This will remove all `asset_names` (whatever their current version)
        from the asset folder.

        Since there might be opened file handle on them, we don't
        delete them but rather put them in a ".old" sibling folder.

        Assets which are not found are ignored.
        """
        old_folder_name = ".old"
        installed = self.get_installed_assets()
        installed = dict((req.asset_name, req) for req in installed)
        for asset_name in asset_names:
            # Skip if not installed yet:
            asset_path = self.assets_path / asset_name
            if not asset_path.exists():
                continue

            # prevent forgetting edit install
            req = installed[asset_name]
            if req.has_edit:
                req.raise_forbidden_with_edit("Protected, cannot uninstall")

            # move install to .old folder with a suffix
            # because opened file handles would prevent unlink
            # and also my religion prevents me from deleting files ^^
            old_folder = asset_path.parent / old_folder_name
            asset_filename = Path(asset_name).name + f"_cq_{time.time()}"
            old_folder.mkdir(parents=False, exist_ok=True)
            shutil.move(self.assets_path / asset_name, old_folder / asset_filename)
            del installed[asset_name]

        # save the installed list
        installed = sorted(installed.values())
        self._save_installed_assets(installed)

    def _save_asset_requirements(self, requirements: Requirement):
        self._settings.asset_requirements = requirements
        self.save_settings()

    def _add_asset_requirements(self, requirements: list[Requirement]):
        # Skip requirement if we already have them, with edit or not:
        # Also raise if another version is required with edit:
        already_required = set(self._settings.asset_requirements)
        edit_required_assets = dict([(req.asset_name, req) for req in already_required])
        new_requirements = set()
        for requirement in requirements:
            if requirement.asset_name in edit_required_assets:
                req = edit_required_assets[requirement.asset_name]
                if (
                    not requirement.has_edit
                    and requirement.version != req.editless_version
                ):
                    requirement.raise_forbidden_with_edit(f"Cannot uninstall {req}")
            if (
                requirement not in already_required
                and requirement.to_edit() not in already_required
            ):
                new_requirements.add(requirement)
        all_requirements = sorted(already_required | new_requirements)
        self._save_asset_requirements(all_requirements)

    def _remove_asset_requirement(self, requirement: Requirement):
        requirements = [
            req for req in self._settings.asset_requirements if req != requirement
        ]
        self._save_asset_requirements(requirements)

    def install_assets(self, *requirements: str | Requirement):
        """
        Add these new requirements to the workspace, then update
        all installed assets to match the requirements.

        """
        self._add_asset_requirements([Requirement(req) for req in requirements])
        self.update_assets()

    def update_assets(self):
        # TODO: maybe have this as argument ?
        # -> if usefull, it would be more on `install_assets()`...
        install_dependencies = True

        # FIXME: this should almost entirely be delegated to the asset_manager !
        # Because checkout, update_checkout (with things from _forget_installed_assets),
        # install_lock, etc... are feature you may want to use
        # outside of a workspace.
        # The workspace is only there to manage a list of requirement/installed.

        # get requirements, we need to update their installed version:
        requirements = self.get_required_assets()

        # Edited asset must no be updated or we loose edits.
        # Let's filter them out:
        to_update = []
        for requirement in requirements:
            must_update = False
            if not requirement.has_edit:
                # Not editable, we check for updates
                must_update = True
            elif not self.has_asset(requirement.asset_name):
                # Editable but not installed, we must install it:
                must_update = True
            elif not self.asset_manager.is_install_editable(
                self.assets_path, requirement
            ):
                # Installed, but not as editable, we must re-install it:
                must_update = True

            if must_update:
                to_update.append(requirement)

        # We'll mess with many files, let's be sure no one is doing
        # the same at the same time:
        with self._install_lock():
            # Current list of installed asset (concrete requirements)
            installed = self.get_installed_assets()

            # Get a list of concrete version which should be installed:
            concrete_requirements = list(self.asset_manager.reduce(*to_update))
            if install_dependencies:
                deps_concrete_requirements = self.asset_manager.resolve_dependencies(
                    concrete_requirements
                )
                # Keep edit for assets required with edit:
                edit_requirements = [req for req in requirements if req.has_edit]
                edit_assets = [req.asset_name for req in edit_requirements]
                concrete_requirements = []
                for req in deps_concrete_requirements:
                    if req.asset_name in edit_assets:
                        req = req.to_edit()
                    if req not in concrete_requirements:
                        concrete_requirements.append(req)
            # Find out which requirement does not match:
            missing_concrete_requirements = [
                req for req in concrete_requirements if req not in installed
            ]

            if 0:  # kept for reference.
                # This was not good: is case of fail while forgeting asset,
                # nothing was installed but some assets were forgotten !
                self._forget_installed_assets(
                    req.asset_name for req in missing_concrete_requirements
                )
                self.asset_manager.install_assets(
                    self.assets_path, *missing_concrete_requirements
                )
            else:
                # This is slower (depending on the index type), but safer:
                for requirement in missing_concrete_requirements:
                    self._forget_installed_assets([requirement.asset_name])
                    self.asset_manager.install_assets(self.assets_path, requirement)

            # update the installed list
            # note: we MUST get it again AFTER the call to
            # `self._forget_installed_assets()` which updates the list !
            installed = self.get_installed_assets()
            installed += missing_concrete_requirements
            self._save_installed_assets(installed)

    def set_editable(self, asset_name: str):
        new_requirement = None
        for requirement in self.get_required_assets():
            if requirement.asset_name == asset_name:
                new_requirement = requirement.to_edit()
                if new_requirement == requirement:
                    # !! this check must be perpormed before _remove_asset_requirement() !!
                    raise ValueError(
                        f"Can't set {asset_name} as editable, it's already editable."
                    )
                self._remove_asset_requirement(requirement)
                break
        if new_requirement is None:
            raise ValueError(
                f"Can't set {asset_name} as editable, it's not found in workspace requirements."
            )
        self.install_assets(new_requirement)

    def import_asset(
        self,
        source_path: Path | None,
        asset_name: str,
        version: int | str = 0,
        index_url: str | None = None,
    ) -> Asset:
        """
        Copy a file or folder into the workspace assets folder
        and flag it as edited so that it can later be published
        as a new version.

        Returns an Asset instance for this new asset.

        If source_path is None, an empty file is created.
        If source_path is '__FOLDER__', an empty folder is created.
        """
        with self._install_lock():
            installed = self.get_installed_assets()
            destination = self.assets_path / asset_name
            if source_path is None:
                destination.parent.mkdir(parents=True)
                with open(destination, "w"):
                    # FIXME: is there a touch() thing in python ? :/
                    pass
            elif source_path == "__FOLDER__":
                destination.mkdir(parents=True)
            else:
                self.asset_manager.copy_asset(source_path, destination, index_url)
            installed.append(
                Requirement.build_version_edit(asset_name, version, index_url=index_url)
            )
            self._save_installed_assets(installed)

        return self.get_asset(asset_name)

    def initialize_file_asset(
        self, asset_name, version: int | str = 0, index_url: str | None = None
    ) -> Asset:
        """
        Creates an empty file asset and flag it as edited so that it can later be
        published as a new version.
        This is a shortcut to `import_asset(source_path=None, ...)`
        """
        return self.import_asset(None, asset_name, version, index_url)

    def initialize_folder_asset(
        self, asset_name, version: int | str = 0, index_url: str | None = None
    ) -> Asset:
        """
        Creates an empty folder asset and flag it as edited so that it can later be
        published as a new version.
        This is a shortcut to `import_asset(source_path="__FOLDER__", ...)`
        """
        return self.import_asset("__FOLDER_", asset_name, version, index_url)

    def asset_filename(self, asset_name: str):
        return self.assets_path / asset_name

    def save_asset_report(self, asset_name: str, report: OperationReport):
        # TODO: persist the report somewhere so we can later refer to it
        # Q: could we publish reports to the asset index while publishing the
        # asset ? would be nice :)
        logger.warning("Report saving not implemented !!!")
        return None

    def publish_asset(
        self,
        install_requirement: Requirement,
        description: str,
        bump_major: bool = False,
        bump_minor: bool = False,
        bump_micro: bool = False,
        flags: set[str] = set(),
    ):
        # We'll mess with the list of installed assets, let's be sure
        # no one is doing the same at the same time or we'll lose some
        # entry in installed file storing the list
        with self._install_lock():
            installed_requirements = self.get_edit_installed_assets()
            if install_requirement not in installed_requirements:
                raise ValueError(
                    f"The requirement {install_requirement!r} was not found in intalled versions. "
                    f"Installed requirements are: {installed_requirements}. "
                    f"Please contact support before trifouilling."
                )
            new_requirement = self.asset_manager.publish(
                self.assets_path,
                install_requirement,
                description,
                bump_major,
                bump_minor,
                bump_micro,
                flags,
            )
            index = installed_requirements.index(install_requirement)
            installed_requirements[index] = new_requirement
            self._save_installed_assets(installed_requirements)

        asset = self.get_asset(install_requirement.asset_name)
