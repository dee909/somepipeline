import sys
from pathlib import Path
import logging

logger = logging.getLogger(__name__)


class Workarea:
    def __init__(self, path: Path):
        self._path = path

    @property
    def user(self) -> str:
        return "blah.user"

    @property
    def current_python(self) -> Path:
        return sys.executable

    @property
    def current_python_version(self) -> tuple[int, int, int]:
        return sys.version_info[:3]
