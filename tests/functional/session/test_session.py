import pytest

from somepipeline.session import Session


@pytest.fixture
def session():
    session = Session("Test Session")
    return session


def test_session_progress(session: Session, capfd):
    progress_name = "Exporting Hair"
    steps = ["Preparing", "Collecting", "Prossessing", "Finalizing"]

    at_step = steps[2]
    session.set_progress(progress_name, at_step, steps)
    # first set prints everything:
    out, err = capfd.readouterr()
    assert not err
    assert session.session_name in out
    assert progress_name in out
    assert at_step in out

    at_step = steps[3]
    session.set_progress(progress_name, at_step, steps)
    # second set in a row doesn't print the header:
    out, err = capfd.readouterr()
    assert not err
    assert session.session_name not in out
    assert progress_name not in out
    # still shows the current step:
    assert at_step in out

    parent_progress = "Export"
    session.set_progress(progress_name, at_step, steps, parent_id=parent_progress)
    # Another progress, with a parent_id:
    out, err = capfd.readouterr()
    assert not err
    assert session.session_name in out
    assert progress_name in out
    assert parent_progress in out
    assert at_step in out

    at_step = steps[3]
    session.set_progress(progress_name, at_step, steps)
    # There was another set printed so we print header again:
    out, err = capfd.readouterr()
    assert not err
    assert session.session_name in out
    assert progress_name in out
    assert at_step in out
