from abc import ABC, abstractmethod
from pathlib import Path
from urllib.parse import urlparse, ParseResult
import logging
import inspect
import platform
import getpass
import time
import os, stat

import pydantic


from somepipeline.plugins import Plugin

from ..requirement import Requirement
from ..version import Version


logger = logging.getLogger(__name__)


# NB: about the asset type and asset tags:
# We could have chosen to have them set on the version
# so that the value can evolve and be tracked during
# asset lifetime.
# We didn't because it would make API more complexe
# and would need a LOT more processing everytime
# for a little amount of usefull times.
# We chose add a log to the asset metadata instead.
# So you can change the tags and the asset_type,
# but you also can see who changed it and when.


class LogEntry(pydantic.BaseModel):
    @classmethod
    def create(cls, level, title, description):
        return cls(
            level=level,
            title=title,
            description=description,
            timestamp=time.time(),
            user=getpass.getuser(),
            node=platform.node(),
        )

    level: int
    title: str
    description: str
    timestamp: float
    user: str
    node: str


class AssetMetadata(pydantic.BaseModel):
    asset_name: str
    asset_type: str | None = None
    tags: set[str] = set()
    versions_inherit_requirements: bool = True
    versions: list[str] = []
    log: list[LogEntry] = []
    custom_data: str | None = None


class VersionMetadata(pydantic.BaseModel):
    asset_name: str
    version: str
    description: str
    flags: set[str] = set()
    requirements: set[Requirement] = set()
    log: list[LogEntry] = []
    custom_data: str | None = None


class AssetIndex(Plugin):
    class DYNAMIC_FLAGS:
        HEAD = "Head"

    class FlagValueError(ValueError):
        def __init__(self, flag, why: str):
            super().__init__(f"Invalid flag value {flag!r}: {why}")

    @classmethod
    def plugin_category(cls):
        return "AssetIndex"

    @classmethod
    @abstractmethod
    def is_plugin_candidat(cls, url_info: ParseResult):
        """
        Subclasses must return True here to be selected
        as the suitable index for the given url info.
        """
        # absctractmethod doen't apply for class method
        # (or at least, I din't get it to work like I wanted)
        # so:
        raise NotImplementedError(getattr(cls, inspect.stack()[0][3]))

    @classmethod
    def version_comp(cls, version: int | str):
        """
        Returns a sortable value from a version string
        """
        return Version(version)._key

    @classmethod
    def sorted_versions(cls, versions: list[str]):
        return sorted(versions, key=cls.version_comp)

    def __init__(self, url) -> None:
        super().__init__()
        self.url = url
        info = urlparse(url)
        self._setup(url, info)

    @abstractmethod
    def _setup(self, url: str, url_info: ParseResult):
        ...

    @classmethod
    def _log_in_metadata(
        cls,
        metadata: AssetMetadata | VersionMetadata,
        title: str,
        description: str,
    ):
        """
        Adds an entry to the log of an AssetMetadata or a VersionMetadata.
        (inplace edit)
        """
        # TODO: actually support level ? or is it overkill ?
        log_entry = LogEntry.create(level=0, title=title, description=description)
        metadata.log.append(log_entry)

    def _on_new_version_created(self, asset_name, version):
        """
        Update all dynamic flags.

        Must be called by subclass implementations every time
        a new version is created.
        """
        update_head = False
        try:
            head_version = self.get_flag_version(asset_name, self.DYNAMIC_FLAGS.HEAD)
        except Requirement.FlagNotFoundError:
            update_head = True
        else:
            # if self.is_version_superior(head_version, version):
            if Version(version) > Version(head_version):
                update_head = True

        if update_head:
            self.flag_version(asset_name, version, self.DYNAMIC_FLAGS.HEAD)

    @abstractmethod
    def get_flags(self, asset_name: str) -> set[str]:
        """
        Return a set with all flags set on this asset.
        """
        ...

    @abstractmethod
    def get_flag_version(self, asset_name: str, flag: str) -> str:
        """
        Returns the name of the version pointed by the given flag.
        """
        ...

    def get_version_flags(self, asset_name: str, version: int | str) -> str:
        """
        Returns the flags pointing to the given version.
        """
        flags = set()
        version = str(version)
        for flag in self.get_flags(asset_name):
            flaged_version = self.get_flag_version(asset_name, flag)
            if flaged_version == version:
                flags.add(flag)
        return flags

    @abstractmethod
    def get_asset_versions(
        self,
        asset_name: str,
        require_operator: str | None = None,
        require_pattern: str | None = None,
    ) -> list[str]:
        ...

    @abstractmethod
    def get_asset_metadata(self, asset_name: str) -> AssetMetadata:
        ...

    @abstractmethod
    def set_versions_inherit_requirements(self, asset_name: str, b: bool):
        ...

    @abstractmethod
    def set_asset_type(self, asset_name: str, asset_type: str):
        ...

    @abstractmethod
    def add_tags(self, asset_name: str, tags: set[str]):
        ...

    @abstractmethod
    def remove_tags(self, asset_name: str, tags: set[str]):
        ...

    @abstractmethod
    def get_version_metadata(
        self, asset_name: str, version: int | str
    ) -> VersionMetadata:
        ...

    @abstractmethod
    def set_version_description(self, asset_name: str, version: str, description: str):
        ...

    @abstractmethod
    def set_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        ...

    @abstractmethod
    def remove_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        ...

    @abstractmethod
    def add_version_requirements(
        self, asset_name: str, version: str, requirements: list[Requirement]
    ):
        ...

    @abstractmethod
    def ingest_content(
        self,
        content: str,
        asset_name: str,
        version: int | str,
        description: str,
    ):
        ...

    @abstractmethod
    def ingest_asset(
        self,
        source_path: Path,
        asset_name: str,
        version: int | str,
        description: str,
    ):
        ...

    @abstractmethod
    def copy_asset(self, source: Path, destination: Path):
        ...

    @abstractmethod
    def has_asset(self, asset_name: str):
        ...

    @abstractmethod
    def has_version(self, asset_name: str, version: int | str):
        ...

    def validate_flag_name(self, flag):
        flag = str(flag)
        if flag.startswith("__"):
            raise self.FlagValueError(flag, "Starting with '__' is reserved.")
        try:
            int(flag)
        except:
            pass
        else:
            raise self.FlagValueError(
                flag, "Flag name cannot be nor look like an integer."
            )

        pointless = flag.replace(".", "")
        try:
            int(pointless)
        except:
            pass
        else:
            raise self.FlagValueError(flag, "Flag name cannot look like an version.")

        invalid_chars = r"!@#$%^&*()<>[]{}\/|"
        for char in invalid_chars:
            if char in flag:
                raise self.FlagValueError(
                    flag,
                    f"Invalid character {char!r}. "
                    f"(Invalid charateres are: {invalid_chars})",
                )
        return flag

    def flag_version(self, asset_name: str, version: int | str, flag: str) -> str:
        """
        Set the flag `flag` on the version `version` of asset `asset_name`.

        The version must exists for the asset.
        The flag value must be a valid flag name, or FlagValueError is raised.
        """
        flag = self.validate_flag_name(flag)
        self._flag_version(asset_name, version, flag)

    @abstractmethod
    def _flag_version(self, asset_name: str, version: int | str, flag: str) -> str:
        ...

    @classmethod
    def _set_read_only(cls, path: Path):
        """
        Recursively set path as read only, in a portable way.

        NOTE: This does pretty much nothing usefull on window...
        (Users are still able to create files in read only folder
        and to rename read only files and folders -__-)
        If you need a better implementation, override this method in your
        AssetIndex subclass. See:
        https://stackoverflow.com/questions/12168110/how-to-set-folder-permissions-in-windows

        """
        if not path.exists():
            raise ValueError(f"Can't set read only on unexistsing path '{path}'.")
        mode = stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH

        if path.is_dir():
            for dirpath, dirnames, filenames in os.walk(path):
                for name in dirnames + filenames:
                    os.chmod(os.path.join(dirpath, name), mode)

        else:
            logger.warning(("RO ON FILE", path))
            os.chmod(path, mode)

    @classmethod
    def _set_read_write(cls, path: Path):
        """
        Recursively set path as read+write, in a portable way.
        """
        if not path.exists():
            raise ValueError(f"Can't set read+write on unexistsing path '{path}'.")

        mode = (
            stat.S_IREAD
            | stat.S_IRGRP
            | stat.S_IROTH
            | stat.S_IWRITE
            | stat.S_IWGRP
            | stat.S_IWOTH
        )

        if path.is_dir():
            for dirpath, dirnames, filenames in os.walk(path):
                for name in dirnames + filenames:
                    os.chmod(os.path.join(dirpath, name), mode)
        else:
            logger.warning(("RW ON FILE", path))
            os.chmod(path, mode)

    @classmethod
    def _is_read_write(cls, path: Path):
        if not path.exists():
            return False
        mode = path.stat().st_mode
        return mode & stat.S_IREAD and mode & stat.S_IWRITE

    def checkout_path(self, asset_name: str, destination: Path) -> Path:
        """
        Return the path of the asset if checkouted at `destination`.
        """
        return destination / asset_name

    @abstractmethod
    def checkout_asset(
        self, asset_name: str, version: int | str, destination: Path, editable: bool
    ) -> Path:
        """
        Checkout this version of the asset under destination.
        Returns the path of the checkouted asset.

        If editable is not True, the resulting file or file tree will be read only.
        """
        ...

    def is_checkout_editable(self, asset_name: str, destination: Path) -> bool:
        """
        Return True if the asset is checkouted at destination with read and
        write permissions.
        """
        return self._is_read_write(self.checkout_path(asset_name, destination))

    def _bump_version(
        self,
        version,
        major: bool = False,
        minor: bool = False,
        micro: bool = False,
    ):
        # FIXME: we should have a class to deal with version -__-
        # -> This is WIP with am.asset_manager.version.Version
        bump_args = (major, minor, micro)
        if True not in bump_args:
            try:
                version = int(version)
            except Exception:
                raise ValueError(
                    f"Can't auto-bump version {version!r}, it's not usable as an integer."
                )
            return version + 1

        return Version(version).bump(major, minor, micro)

    def checkin_asset(
        self,
        asset_name: str,
        source: Path,
        description: str,
        original_version: int | str,
        bump_major: bool = False,
        bump_minor: bool = False,
        bump_micro: bool = False,
        flags: list[str] | tuple[str] | set[str] = [],
    ) -> str:
        """
        Will create a new version for this asset using the content at `source` path.
        Returns the name of the created version.

        The new version will depend on the bump_major, bump_minor, and bumpt_patch args.
        Those 3 arguments are mutually exclusive. If all 3 are False, the version is
        considerer as an integer to increase.

        The new version will have the given flag set to it.
        (nb: the head flag must not be provided, it is managed by the index.)
        """
        # TODO: add a "created_from_version" field in VersionMetadata and store:
        # original_version + bump_* in it.
        version = self._bump_version(
            original_version, bump_major, bump_minor, bump_micro
        )
        self.ingest_asset(source, asset_name, version, description)
        for flag in flags:
            self.flag_version(asset_name, version, flag)

        return str(version)
