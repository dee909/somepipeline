import pytest

from somepipeline.am.workspace import Workspace, WorkspaceSettings, Requirement, Asset

from . import plugin_utils


@pytest.fixture
def workspace(tmp_path):
    """
    Return a workspace connected to a memory index
    containing asset:
        CHARS/Main/Bob/Bob-actor.ma
        SETS/Kitchen/Kitchen-set.ma
        PROPS/Kitchen/Table/Kitchen_Table-actor.ma
        PROPS/Kitchen/Chair/Kitchen_Chair-actor.ma
    """
    BLESSED = "Blessed"  # yeah I know, this is the default anyway...
    settings = WorkspaceSettings()
    settings.asset_manager.default_flag = BLESSED
    settings.asset_manager.default_index.url = "memory://test_index"
    workspace = Workspace.create(tmp_path / "MyWorkspace", settings, create_venv=False)

    versions = (1, 2, 3, 4, 5)
    asset_names = (
        "CHARS/Main/Bob/Bob-actor.ma",
        "SETS/Kitchen/Kitchen-set.ma",
        "PROPS/Kitchen/Table/Kitchen_Table-actor.ma",
        "PROPS/Kitchen/Chair/Kitchen_Chair-actor.ma",
    )
    for asset_name in asset_names:
        for version in versions:
            content = (
                f"\n# This is the content for asset {asset_name} at version {version}."
            )
            workspace.asset_manager.ingest_content(
                content, asset_name, version, "test content..."
            )
        workspace.asset_manager.flag_version(asset_name, version, BLESSED)
    return workspace


def test_asset_manipulation(workspace: Workspace):
    # This install dummy asset plugin for maya scene
    # and maya work scene. They rely on the asset extension
    # being `.ma` or `.mb`
    MayaSceneAsset, MayaWorkScene = plugin_utils.install_maya_demo_asset_plugins()

    # We have a brand new workspace, an asset index filled with
    # assets, and we have a task to perform.
    # We'll need those assets to work on this task:
    bob = "CHARS/Main/Bob/Bob-actor.ma"
    kitchen = "SETS/Kitchen/Kitchen-set.ma"
    table = "PROPS/Kitchen/Table/Kitchen_Table-actor.ma"
    chair = "PROPS/Kitchen/Chair/Kitchen_Chair-actor.ma"

    # Let's install them:
    workspace.install_assets(bob, kitchen, table, chair)

    # By default, the installed version is the 'Blessed' one.
    # In our situation, the blessed is the version 5 for
    # every asset.
    # Let's inspect our assets:
    for asset in workspace.get_assets():
        # Each asset knows its requirement:
        assert asset.requirement().flag is None
        assert asset.requirement().version is None
        # And the corresponding installe version
        assert asset.installed_version() == "5"

    # Now we need a scene to work on:
    # (note that our demo Asset plugins need the filename to contain
    # the "Task" string in order to be detected as a work scene)
    work_scene = "Film/Sq001/Sh001/Sq001_Sh001-AnimTask.ma"

    # Maybe someone already worked on this task, or maybe
    # we're the first workspace to do it ?
    # In the first case we probably want to work on the last
    # scene published for the task.
    # In the other case, we need to create a new asset and
    # initialize it.
    # Let's ask the workspace's asset manager:
    asset_exists = workspace.asset_manager.has_asset(work_scene)
    assert not asset_exists

    # It appears we're alone on the task... Let's create
    # an asset and build it.

    # Since we don't start from an existing version, we create
    # a new asset for our work:
    work_asset = workspace.initialize_file_asset(work_scene)

    # The type of asset we created depends on the Asset plugin
    # installed in the system. In our case, it's a:
    assert type(work_asset) == MayaWorkScene
    assert type(work_asset) != MayaSceneAsset
    assert isinstance(work_asset, Asset)

    # The operation you can perfom on an assert depends on
    # the asset type, and the context you'll run them in.
    # (the context tells for example if you're in a dcc, on
    # the farm, on mars...)
    # In our situation, the context does not matter so we can
    # use an unconfigured context:
    context = work_asset.OperationContext("Just testing")
    operations = work_asset.available_operations(context)
    assert set(operations) == set(["build", "check", "export"])

    # LET'S BUILD !
    # Note: We create a report to get a hold on it, but this is
    # not mandatory (reports are saved alongside assets and
    # will be published uppon checkin)
    # TODO: actually save the report alongside the asset ^^
    report = Asset.OperationReport("Demo Build")

    # NOW LET'S ACTUALLY BUILD !
    work_asset.perform_operation("build", report)

    # The report will show everything that happened during
    # the operation. Every involved asset participates by
    # adding debug, info, warning, error or critical events
    # (yes, those match the levels in the logging module).

    # If everything went fine enough, the report show success:
    # (that is, there's nothing above warning in it)
    assert report.success

    # You can pretty print the report to see it all:
    # (use pytest `-s` command line flag to show this print)
    print(report.pformat())
