from urllib.parse import ParseResult
from somepipeline.am.asset_manager.asset_index.asset_index_plugin import AssetIndex


class MongoAssetIndex(AssetIndex):
    @classmethod
    def is_plugin_candidat(cls, url_info: ParseResult):
        return url_info.scheme == "mongoassets"
