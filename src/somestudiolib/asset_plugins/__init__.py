from somepipeline.plugins import PluginRegistry

from .blender import install_blender_plugins


def install_asset_plugins(plugin_registry: PluginRegistry):
    install_blender_plugins(plugin_registry)
