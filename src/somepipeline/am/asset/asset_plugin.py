from __future__ import annotations

from typing import TYPE_CHECKING, Protocol
from pathlib import Path
import logging

if TYPE_CHECKING:
    from ..workspace import Workspace

from somepipeline.plugins import Plugin, abstractmethod

from ..asset_manager.requirement import Requirement
from .asset_operations import OperationContext, OperationReport

logger = logging.getLogger(__name__)


class AssetOperationCallable(Protocol):
    """
    Signature for an Assert Operator callable.
    The return value indicates if the operation should stop (only
    affects chained operations)
    """

    def __call__(self, context: OperationContext, report: OperationReport) -> bool:
        ...


class Asset(Plugin):
    class EXCEPTIONS:
        class AssetOperationError(Exception):
            pass

        class BuildError(AssetOperationError):
            pass

        class CheckError(AssetOperationError):
            pass

        class ExportError(AssetOperationError):
            pass

    # for convenience:
    OperationReport = OperationReport
    OperationContext = OperationContext

    @classmethod
    def plugin_category(cls):
        return "Asset"

    @classmethod
    def asset_type_name(cls):
        return cls.plugin_type_name()

    @classmethod
    @abstractmethod
    def is_plugin_candidat(
        cls,
        asset_name: str,
        asset_type_name: str,
        asset_tags: tuple[str],
    ):
        """
        Subclasses must return True here to be selected
        as the suitable Asset class for the given info.

        The candidats argument contains the asset which
        answered yes so far. Subclasses may dedide to back
        off it there's already a candidat (return False), or
        to modify the candidats list content
        (maybe with `candidats.clear()` or candidats[:-1]=[]).
        """
        # Be sure to raise a informative exception:
        super().is_plugin_candidat()

    @classmethod
    def plugin_election(
        cls,
        candidats: set[Plugin],
        asset_name: str,
        asset_type_name: str,
        asset_tags: tuple[str],
    ):
        # This is just so that subclass forgeting to implement this method will raise
        # a nice informative exception:
        return super().plugin_election(
            candidats, asset_name, asset_type_name, asset_tags
        )

    def __init__(self, workspace: Workspace, asset_name: str):
        self._workspace = workspace
        self._asset_name = asset_name

    @property
    def workspace(self):
        return self._workspace

    @property
    def asset_name(self):
        return self._asset_name

    def filename(self) -> Path:
        return self.workspace.asset_filename(self._asset_name)

    def save_report(self, report):
        report_filename = self.workspace.save_asset_report(self.asset_name, report)
        return report_filename

    def installed_version(self) -> str | None:
        """
        Returns the version installed in the workspace.
        Is always concrete.
        """
        #! do not cache this value, or find a way to update
        # all Asset instances after a new version is published...
        installed = self.workspace.get_installed_assets()
        for requirement in installed:
            if requirement.asset_name == self._asset_name:
                return requirement.version
        return None

    def requirement(self) -> Requirement | None:
        """
        Returns the requirement for this asset in the workspace.
        May be abstract.
        """
        requirements = self.workspace.get_required_assets()
        for requirement in requirements:
            requirement = Requirement(requirement)
            if requirement.asset_name == self._asset_name:
                return requirement
        return None

    def install(self, version: int | str, editable=False, index_url: str | None = None):
        """
        Installs the given version, removing any previously installed one.
        """
        if editable:
            requirement = Requirement.build_version_edit(
                self._asset_name, version, index_url
            )
        else:
            requirement = Requirement.build_version(
                self._asset_name, version, index_url=index_url
            )
        self.workspace.install_assets(requirement)

    def publish(
        self,
        description: str,
        bump_major: bool = False,
        bump_minor: bool = False,
        bump_micro: bool = False,
        flags: set[str] = set(),
    ):
        self.workspace.publish_asset(
            self.installed_version(),
            description,
            bump_major,
            bump_minor,
            bump_micro,
            flags,
        )

    def available_operations(self, context: OperationContext) -> list[str]:
        """
        Subclasses should override this to return the name of operations
        suitable for the given context.

        The returned values must be handlable by `_get_operation()`.
        """
        return ["build", "check", "export"]

    def _get_operation(self, operation_name: str) -> AssetOperationCallable:
        """
        Returns a callable with signature:
            `(context: OperationContext, report: OperationReport) -> bool`

        Default implementation is to call the method named `operation_name`
        Subclassses must override this to matche overrides on `available_operations()`.
        """
        try:
            operation = getattr(self, operation_name)
        except TypeError:
            err = TypeError(
                f'Asset "{self.asset_type_name()}" '
                'does not support "{operation_name}" operation.'
            )
            raise err
        return operation

    def create_operation_context(self, operation_name: str):
        """
        Call before performing an operation from the asset.
        Subclasses may want to specialize this.
        """
        return OperationContext(operation_name)

    def perform_operation(
        self, operation_name: str, report: OperationReport | None = None
    ) -> tuple[bool, str]:
        """
        Runs the given operation with the given context.

        with the given context.

        Returns (sucess_bool, report_filename)

        Subclasses may want to instanciate a specialized context and
        use it instead.
        """
        report = report or OperationReport(
            f"Performing {operation_name} on {self.asset_name}"
        )
        with report.section("Using context:") as report_section:
            context = self.create_operation_context(operation_name)
            report_section.object(context)
        with report.exceptions("While finding operation method: "):
            operation = self._get_operation(operation_name)
        with report.exceptions("While running operation: ", Exception):
            stop_requested = operation(context, report)
        if stop_requested:
            report.warning(
                f"{operation_name} on {self.asset_name} returned {stop_requested}, "
                "(it's the end anyway ^^)"
            )

        report_filename = self.save_report(report)
        print(report.pformat())
        return report.success, report_filename

    @abstractmethod
    def build(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Build this asset in the given context.
        """
        ...

    @abstractmethod
    def check(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Check the context and report any issue regarding this asset.
        """
        ...

    @abstractmethod
    def export(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Export the content of this asset from the given context.
        """
        ...
