from somepipeline.am.asset.asset_plugin import Asset
from somepipeline.am.asset.asset_operations import OperationContext, OperationReport

from somepipeline.plugins import plugin_registry, Plugin


#
#   EveryAsset Plugin
#
class EveryAsset(Asset):
    @classmethod
    def is_plugin_candidat(
        cls,
        asset_name: str,
        asset_type_name: str,
        asset_tags: list[str],
    ):
        # This Asset type supports everything:
        return True

    # Mandatory implementations:
    def build(self, context: OperationContext, report: OperationReport) -> bool:
        return

    def check(self, context: OperationContext, report: OperationReport) -> bool:
        return

    def export(self, context: OperationContext, report: OperationReport) -> bool:
        return


def install_generic_asset_plugin():
    plugin_registry.add_plugin(EveryAsset)


#
#   Maya example Asset plugins
#


class MayaSceneAsset(Asset):
    """
    This is for demo/doc purpose. You will need to implement
    your own assets with your custom behavior.

    Your asset should be more specialized than this one.
    For example:
        AnimCacheAsset
        ModAsset
    rather than:
        ABCAsset
        OBJAsset

    Remember: your assets do not represent some file type but
    the usage of some files (your `build()` and `export()` implementations
    can be different for a single file extension).
    """

    @classmethod
    def is_plugin_candidat(
        cls,
        asset_name: str,
        asset_type_name: str,
        asset_tags: list[str],
    ):
        return asset_name.endswith(".ma") or asset_name.endswith(".mb")

    @classmethod
    def plugin_election(
        cls,
        candidats: set[Plugin],
        asset_name: str,
        asset_type_name: str,
        asset_tags: tuple[str],
    ):
        # we're only the last option
        # if anything else was ok to handle it, let them handle it:
        # so we keep racing for election only if we are the sole candidat:
        if candidats != {cls} and cls in candidats:
            candidats.remove(cls)

    def build(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Add a ref to this scene in the current scene.
        """
        if context.dcc_name != "maya":
            raise self.EXCEPTIONS.BuildError("Can only build inside maya.")

        filename = self.filename()
        cmds = context.maya_cmds
        cmds.file(filename, reference=True)
        report.info(f"Reference created for {filename.name}.")

    def check(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Check the context and report any issue regarding this asset.
        """
        raise NotImplementedError()

    def export(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Export the content for this asset to a new scene.
        """
        # well... I don't have maya right now and I don't know how to
        # export something as a maya scene and I'm too lazy to look it up
        # so let's just pretend this content is good enough:
        report.info("Faking it...")
        with report.exception(catch=False):
            content = "\n# This is an exported maya scene. Trust me.\n"
            with open(self.filename(), "w") as fp:
                fp.write(content)


class MayaWorkScene(Asset):
    """
    A maya scene where talents will work using other assets to produce
    new assets.
    """

    @classmethod
    def is_plugin_candidat(
        cls,
        asset_name: str,
        asset_type_name: str,
        asset_tags: list[str],
    ):
        # support maya scene with "Task" in asset type name or filename:
        supported = (asset_name.endswith(".ma") or asset_name.endswith(".mb")) and (
            "Task" in asset_type_name or "Task" in asset_name
        )
        return supported

    @classmethod
    def plugin_election(
        cls,
        candidats: set[Plugin],
        asset_name: str,
        asset_type_name: str,
        asset_tags: tuple[str],
    ):
        # If we're racing for election, we win the race:
        if cls in candidats:
            candidats.clear()
            candidats.add(cls)

    def build(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Builds all asssets in the current scene.
        """
        try:
            import maya.cmds  # type: ignore
        except ImportError:
            # all this is just a mockup, let's fake it ^_^
            class maya:
                class cmds:
                    @classmethod
                    def file(cls, filename, *args, **kwargs):
                        print(f"Machallah! we're soooo referencing {filename} !!!")

        context.dcc_name = "maya"  # fake it until we make it ! ^^
        context.maya_cmds = maya.cmds
        for asset in self._workspace.get_assets():
            if asset is self:
                continue
            with report.asset_section(asset) as report_section:
                stop = asset.build(context, report_section)
            if stop:
                return stop

    def check(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Check all asssets in the current scene.
        """
        for asset in self._workspace.get_assets():
            if asset is self:
                continue
            with report.asset_section(asset) as report_section:
                stop = asset.check(context, report_section)
            if stop:
                return stop

    def export(self, context: OperationContext, report: OperationReport) -> bool:
        """
        Export all assets from the current scene.
        """
        # we want to export everything exportable from the scene:
        #   - get all asset plugins supporting export from this
        #       - either from plugin registry
        #       - or from a list defined in the code
        #   - make them create instance for every Asset they can export from the scene
        #   - install the corresponding assets as edited in the workspace
        #   - run the export on each asset
        asset_types: type[Asset] = []  # FIXME: implement this !
        for asset_type in asset_types:
            assets_to_export: list[Asset] = asset_type.create_assets_to_export(
                context,
                report.section(f"Lookup exportable by {asset_type.asset_type_name}"),
            )
        for asset in assets_to_export:
            if asset is self:
                raise ValueError(
                    "Explorting myself would loop idefinitely "
                    f"({asset.asset_name}, {asset})."
                )
            with report.asset_section(asset) as report_section:
                stop = asset.export(context, report_section)
            if stop:
                return stop


def install_maya_demo_asset_plugins():
    plugin_registry.add_plugins(MayaSceneAsset, MayaWorkScene)
    return MayaSceneAsset, MayaWorkScene  # used for type assertion
