from pathlib import Path
import logging
import json

import pytest

from somepipeline.am.workspace import Workspace, WorkspaceSettings

from . import plugin_utils, asset_index_utils

logging.basicConfig(level=logging.INFO)


def prepare_asset(
    tmp_path: Path, workspace: Workspace, asset_name, *asset_versions, **flag_versions
):
    asset_manager = workspace.asset_manager

    foreign_folder = tmp_path / "_prepare_asset"

    for version in asset_versions:
        foreign_file = foreign_folder / f"foreign_{asset_name}_v{version}.txt"
        foreign_file.parent.mkdir(parents=True, exist_ok=True)
        with open(foreign_file, "w") as fp:
            fp.write(f"This is a prepared asset for test: {asset_name=} {version=}")
        asset_manager.ingest_asset(
            foreign_file, asset_name, version, "mandatory description"
        )

    for flag, version in flag_versions.items():
        asset_manager.flag_version(asset_name, version, flag)


@pytest.fixture
def quick_workspace(tmp_path: Path):
    asset_store = tmp_path / "ASSET_STORE"
    workspace_dir = tmp_path / "WorkArea" / "WorkSpace"

    settings = WorkspaceSettings()
    settings.asset_manager.default_index.url = f"lan://{asset_store}"
    workspace = Workspace.create(workspace_dir, settings, create_venv=False)

    return workspace


def disabled_test_create(tmp_path: Path):
    # too long beacause of default venv creation
    # TODO: move test with venvs in a separated test module

    workspace_dir = tmp_path / "WorkArea" / "WorkSpace"

    settings = WorkspaceSettings()
    workspace = Workspace.create(workspace_dir, settings)

    assert workspace.exists()
    assert workspace.py_path.exists()
    assert workspace.assets_path.exists()
    assert workspace.settings_filename.exists()
    assert workspace.python.exists()

    import os

    # os.system(str(workspace.python))


def test_load(quick_workspace: Workspace):
    prev_workspace = quick_workspace

    workspace = Workspace(prev_workspace.path)
    assert workspace._settings == prev_workspace._settings


def test_install_asset(quick_workspace: Workspace, tmp_path: Path):
    workspace = quick_workspace

    asset_name = "PROPS/Kitchen/Chair/KitchenChair-mod_scene.ma"
    prepare_asset(tmp_path, workspace, asset_name, 1, 2, "2.1", 3, Blessed="2.1")

    require = f"{asset_name}@Head"
    workspace.install_assets(require)
    assert f"{asset_name}@Head" in workspace.get_required_assets()
    assert workspace.get_installed_assets() == [f"{asset_name}==3"]

    asset_name = "PROPS/Kitchen/Glass/KitchenGlass-mod_scene.ma"
    prepare_asset(tmp_path, workspace, asset_name, 1, 2, "2.1", 3, Blessed="2.1")

    require = f"{asset_name}@Blessed"
    workspace.install_assets(require)
    assert f"{asset_name}@Blessed" in workspace.get_required_assets()
    assert f"{asset_name}==2.1" in workspace.get_installed_assets()

    # Test make already installed asset editable
    workspace.set_editable(asset_name)
    assert f"{asset_name}==2.1+edit" in workspace.get_installed_assets()
    assert f"{asset_name}==2.1+edit" in workspace.get_edit_installed_assets()
    with pytest.raises(ValueError):
        workspace.set_editable(asset_name)
    assert workspace.has_asset(asset_name)
    assert f"{asset_name}@Blessed+edit" in workspace.get_required_assets()
    assert f"{asset_name}@Blessed+edit" in workspace.get_edit_required_assets()
    assert f"{asset_name}==2.1+edit" in workspace.get_installed_assets()
    assert f"{asset_name}==2.1+edit" in workspace.get_edit_installed_assets()

    # Test install new asset editable
    asset_name = "TEST/test_editable_install.ma"
    prepare_asset(tmp_path, workspace, asset_name, 1, 2, 3, Blessed="2")
    require = f"{asset_name}==3+edit"
    workspace.install_assets(require)
    assert f"{asset_name}==3+edit" in workspace.get_required_assets()
    assert f"{asset_name}==3+edit" in workspace.get_edit_required_assets()
    assert f"{asset_name}==3+edit" in workspace.get_installed_assets()
    assert f"{asset_name}==3+edit" in workspace.get_edit_installed_assets()


def test_import_asset(quick_workspace: Workspace, tmp_path: Path):
    foreign_folder = tmp_path / "some_foreign_folder"
    foreign_asset = foreign_folder / "foreign_movie_info.json"
    foreign_asset.parent.mkdir(parents=True)
    data = dict(title="The Title", lenght=90, status="seen")
    with open(foreign_asset, "w") as fp:
        json.dump(data, fp)

    workspace = quick_workspace

    # Import asset will try to create an Asset instance for the imported asset
    # Let's register a generic Asset type to handle it:
    plugin_utils.install_generic_asset_plugin()

    # Now we can import the asset:
    asset = workspace.import_asset(foreign_asset, "DATA/Movies/MovieName.json")

    # The returned value is an Asset instance.
    # But we'll talk about that later ;)
    from somepipeline.am.asset.asset_plugin import Asset

    assert isinstance(asset, Asset)


def test_workspace_install_with_dependencies(
    quick_workspace: Workspace, tmp_path: Path
):
    am = quick_workspace.asset_manager
    default_flag = "VALIDATED"
    am.set_default_flag(default_flag)
    if 0:
        index_url = "memory://test_index"
        am.set_default_index(index_url)
    index = am._get_index(None)  # get default index

    # Let's populate the index with these dependencies:
    # Mod <-- Lookdev <----------------------------- Lighting
    #     <-------------- Rig <-- Actor <-- Anim <--/ /
    #     <-------------- UVs <--/                   /
    #                         <---------------------/

    create_versions = asset_index_utils.asset_index_create_versions
    create_versions(index, "Lighting", 12, {}, "Lookdev@Blessed", "Anim", "UVs")
    create_versions(index, "Anim", 1, {default_flag}, "Actor@Blessed")
    create_versions(index, "Actor", 1, {"Blessed"}, "Rig@Head", "UVs")
    create_versions(index, "Rig", 18, {}, "Mod")
    create_versions(index, "UVs", 22, {default_flag}, "Mod")
    create_versions(index, "Lookdev", 11, {}, "Mod")
    create_versions(index, "Lookdev", 12, {"Blessed"}, "Mod")
    create_versions(index, "Mod", 13, {default_flag})

    # lets install Lighting:
    quick_workspace.install_assets("Lighting@Head")
    # check requirement:
    expected = {"Lighting@Head"}
    assert set(quick_workspace.get_required_assets()) == expected
    # check everything is installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1",
        "Rig==18",
        "UVs==22",
        "Lookdev==12",
        "Mod==13",
    }
    # we don't assert order:
    assert set(installed) == expected

    # Now let's ask for one of them as editable:
    quick_workspace.install_assets("Actor@Blessed+edit")
    # check requirement:
    expected = {"Lighting@Head", "Actor@Blessed+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1+edit",
        "Rig==18",
        "UVs==22",
        "Lookdev==12",
        "Mod==13",
    }
    assert set(installed) == expected

    # And let's ask for update:
    quick_workspace.update_assets()
    # check requirement:
    expected = {"Lighting@Head", "Actor@Blessed+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1+edit",
        "Rig==18",
        "UVs==22",
        "Lookdev==12",
        "Mod==13",
    }
    assert set(installed) == expected

    # lets try one more, without using a flag
    quick_workspace.install_assets("Lookdev==12+edit")
    # check requirement:
    expected = {"Lighting@Head", "Actor@Blessed+edit", "Lookdev==12+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1+edit",
        "Rig==18",
        "UVs==22",
        "Lookdev==12+edit",
        "Mod==13",
    }
    assert set(installed) == expected

    # When installing something w/o edit but we already have it with edit,
    # we must keep the edits:
    quick_workspace.install_assets("Lookdev==12")
    # Check requirement:
    expected = {"Lighting@Head", "Actor@Blessed+edit", "Lookdev==12+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1+edit",
        "Rig==18",
        "UVs==22",
        "Lookdev==12+edit",
        "Mod==13",
    }
    assert set(installed) == expected

    # When adding something w/o edit but we already have it with edit
    # at another version, we must raise:
    with pytest.raises(ValueError):
        # Note: the error is actually a Requirement.ForbiddenWithEditError():
        quick_workspace.install_assets("Lookdev==11")
    # The install has been stopped, and our requirement stayed the same:
    expected = {"Lighting@Head", "Actor@Blessed+edit", "Lookdev==12+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1+edit",
        "Rig==18",
        "UVs==22",
        "Lookdev==12+edit",
        "Mod==13",
    }
    assert set(installed) == expected

    # Let's say someone else created a new version of one
    # of our indirect dependency, and added a dependency
    # to another asset.
    create_versions(index, "AnotherActor", 1, {"Blessed"})
    create_versions(
        index, "Anim", 2, {default_flag}, "Actor@Blessed", "AnotherActor@Blessed"
    )
    # Now an update on our workspace should install this
    # new version, and thus the new asset too:
    quick_workspace.update_assets()
    # Check requirements did not change:
    expected = {"Lighting@Head", "Actor@Blessed+edit", "Lookdev==12+edit"}
    assert set(quick_workspace.get_required_assets()) == expected
    # Check installed:
    installed = quick_workspace.get_installed_assets()
    expected = {
        "Lighting==12",
        "Anim==2",
        "Actor==1+edit",
        "AnotherActor==1",
        "Rig==18",
        "UVs==22",
        "Lookdev==12+edit",
        "Mod==13",
    }
    assert set(installed) == expected
