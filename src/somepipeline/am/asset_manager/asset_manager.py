from typing import Iterable
from pathlib import Path
import logging
from urllib.parse import urlparse, ParseResult

from somepipeline.plugins import plugin_registry
from .requirement import Requirement
from .asset_index.asset_index_plugin import AssetIndex, AssetMetadata, VersionMetadata


logger = logging.getLogger(__name__)


class AssetManager:
    def __init__(self) -> None:
        self._default_flag = "Blessed"
        self._indexes: dict[str, AssetIndex] = {}

    def set_default_flag(self, flag: str):
        """
        Set the flag to use when a requirement only has the asset name.
        The init default is "Blessed", a nice alternative would be "Head"
        (or whatever dynamic flag your configured index supports), but
        you can set any string you want.
        """
        self._default_flag = flag

    def set_index_alias(self, alias: str, index_url: str):
        index = self._get_index(index_url)
        self._indexes[alias] = index

    def set_default_index(self, index_url: str):
        index = self._get_index(index_url)
        self._indexes[None] = index

    def _get_index(self, url: str) -> AssetIndex:
        try:
            return self._indexes[url]
        except KeyError:
            # let's create it:
            pass

        info = urlparse(url)

        asset_index_type = plugin_registry.elect_plugin("AssetIndex", info)
        asset_index = asset_index_type(url)
        self._indexes[url] = asset_index
        return asset_index

    def get_dependency_requirements(self, requirement: Requirement):
        # FIXME: do we like this funciton name ?
        # TODO: this reduce being a generator is kind of annoying -__-
        concrete = list(self.reduce(requirement))[0]
        index = self._get_index(concrete.index_url)
        # FIXME: ! CRITICAL: we're getting metadata from the index, but if the
        # version is installed in the workspace it may have its metadata
        # edited !!!
        md = index.get_version_metadata(concrete.asset_name, concrete.version)
        return md.requirements

    def resolve_dependencies(self, requirements: list[Requirement]) -> set[Requirement]:
        """
        Returns a list of concrete requirements needed by the given
        requirements and all their dependency requirements.
        """
        resolved = set([])
        for requirement in requirements:
            # TODO: this reduce being a generator is kind of annoying -__-
            concrete = list(self.reduce(requirement))[0]
            self._resolve_dependencies(concrete, resolved)
        return resolved

    def _resolve_dependencies(
        self, requirement: Requirement, resolved: set[Requirement]
    ):
        if requirement in resolved:
            return
        resolved.add(requirement)
        dependencies = self.get_dependency_requirements(requirement)
        dependencies = set(self.reduce(*dependencies))
        if 0:
            # We cannot just remove resolved because +edit should
            # match not +edit
            unresolved = dependencies - resolved
        else:
            edit_resolved = [req.to_edit() for req in resolved]
            unresolved = [
                req for req in dependencies if req.to_edit() not in edit_resolved
            ]
        for req in unresolved:
            self._resolve_dependencies(req, resolved)

    def reduce(self, *requirements: Requirement) -> Iterable[Requirement]:
        """
        Generator of concrete and valid requirements from the given list
        of requirements.
        Each nth yielded requirement correspond to the nth entry of the given
        requirements.

        The returned requirements will all have a '==' operator and an exising
        version for the required asset_name.

        The default asset index is used for requirement w/o specified index.
        Index aliases and arbitrary index url are supported.

        Raises `Requirement.AssetNotFoundError`, `Requirement.FlagNotFoundError`
        and `Requirement.VersionNotFoundError`.
        """
        # TODO: requirements being a *args, we might want to check that it
        #  is not ([...],) and raise an informative value error.
        # TODO: just dont use *arg for requirements
        # TODO: check if the requirement is already concrete ?
        # ---> NO ! We still want to check the version exists.
        for requirement in requirements:
            requirement = Requirement(requirement)  # coerce...
            index = self._get_index(requirement.index_url)
            asset_name = requirement.asset_name
            if not index.has_asset(asset_name):
                requirement.raise_asset_not_found()

            if requirement.has_name_only:
                requirement = requirement.build_flag(
                    asset_name, self._default_flag, requirement.index_url
                )

            if requirement.has_flag:
                version = index.get_flag_version(asset_name, requirement.editless_flag)

            elif requirement.is_concrete:
                version = requirement.editless_version
                if not index.has_version(asset_name, version):
                    requirement.raise_version_not_found()
            elif requirement.is_abstract:
                versions = index.get_asset_versions(
                    asset_name,
                    requirement.operator,
                    requirement.pattern,
                )
                if versions:
                    # get the highest selected version:
                    version = versions[-1]
                else:
                    requirement.raise_version_not_found()
            else:
                # This should not be possible, like my divorce...
                raise RuntimeError("WTF ?!?")
            if requirement.has_edit:
                yield Requirement.build_version_edit(
                    asset_name, version, requirement.index_url
                )
            else:
                yield Requirement.build_version(
                    asset_name, version, requirement.index_url
                )

    def has_asset(self, asset_name: str, index_url: str | None = None):
        """
        Return True if asset_name exists in the asset index.
        """
        index = self._get_index(index_url)
        return index.has_asset(asset_name)

    def get_asset_versions(self, requirement: Requirement) -> list[VersionMetadata]:
        """
        Return an ordererd list of versions for the given asset.
        """
        index = self._get_index(requirement.index)
        return index.get_asset_versions(requirement.asset_name)

    def ingest_asset(
        self,
        source_path: Path,
        asset_name: str,
        version: int | str,
        description: str,
        index_url: str | None = None,
    ):
        """
        Create the specified asset version from the given source path.

        This is usefull when populating from an external versionning system
        where the version names are already known.

        In order to publish a new asset version, you should rather use `publish_asset()`

        """
        index = self._get_index(index_url)
        return index.ingest_asset(source_path, asset_name, version, description)

    def ingest_content(
        self,
        content: str,
        asset_name: str,
        version: int | str,
        description: str,
        index_url: str | None = None,
    ):
        """
        Create the specified asset version with the given content.

        See `ingest_asset()`
        """
        index = self._get_index(index_url)
        return index.ingest_content(content, asset_name, version, description)

    def copy_asset(self, source: Path, destination: Path, index_url: str | None = None):
        """
        Copy anything that could be an asset from source to destination.

        The default asset index is used if no `index_url` is specified.
        Index aliases and arbitrary index url are supported.

        The selected index may adapt the source to a suitable form for a later check-in.
        """
        index = self._get_index(index_url)
        index.copy_asset(source, destination)

    def flag_version(
        self, asset_name: str, version: str, flag: str, index_url: str | None = None
    ):
        index = self._get_index(index_url)
        index.flag_version(asset_name, version, flag)

    def install_assets(self, install_folder: Path, *requirements: Requirement):
        """
        All requirements must be concreate. see `reduce()`.
        """
        # TODO: requirements being a *args, we might want to check that it
        # is not ([...],) and raise an informative value error.
        for requirement in requirements:
            requirement = Requirement(requirement)  # coerce
            if not requirement.is_concrete:
                raise ValueError(
                    f"The requirement {requirement} is not concrete, cannot checkout."
                )
            index = self._get_index(requirement.index_url)
            index.checkout_asset(
                requirement.asset_name,
                requirement.editless_version,
                install_folder,
                requirement.has_edit,
            )

    def install_path(
        self, install_folder: Path, install_requirement: Requirement
    ) -> Path:
        """
        Install the asset under install_folder as described in install_requirement.
        """
        install_requirement = Requirement(install_requirement)
        index = self._get_index(install_requirement.index_url)
        return index.checkout_path(install_requirement.asset_name, install_folder)

    def is_install_editable(self, install_folder: Path, requirement: Requirement):
        index = self._get_index(requirement.index_url)
        return index.is_checkout_editable(
            requirement.asset_name,
            install_folder,
        )

    def publish(
        self,
        install_folder: Path,
        install_requirement: Requirement,
        description: str,
        bump_major: bool = False,
        bump_minor: bool = False,
        bump_micro: bool = False,
        flags: set[str] = set(),
    ) -> str:
        """
        Publish the given asset as bumped version.
        Returns the requirement str for the new version.
        """
        # FIXME: ! we should have access to the installed version metadata, and
        # we should get in them 'original_version' for AssetIndex.checkin_asset() !
        install_requirement = Requirement(install_requirement)
        index = self._get_index(install_requirement.index_url)
        new_version = index.checkin_asset(
            install_requirement.asset_name,
            self.install_path(install_folder, install_requirement),
            description,
            install_requirement.editless_version,
            bump_major,
            bump_minor,
            bump_micro,
            flags,
        )
        logger.warning((install_requirement.asset_name, new_version, index.url))
        # FIXME: we assume the original requirement has edit bc you probably edit
        # before publishing, but in the case of just "retop old version" it wont
        # be true and the user will think the install is in edit mode but it's not !
        new_install_requirement = Requirement.build_version_edit(
            install_requirement.asset_name, new_version, install_requirement.index_url
        )
        return new_install_requirement
