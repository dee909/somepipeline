from pathlib import Path

from somepipeline.am.asset_manager.asset_index.asset_index_plugin import AssetIndex

# from somepipeline.am.asset_manager.asset_index.lan import LANAssetIndex
from somepipeline.am.asset_manager import AssetManager
from somepipeline.am.asset_manager.requirement import Requirement

from .asset_index_utils import asset_index_create_versions


def test_asset_manager_get_index(tmp_path: Path):
    storage = tmp_path / "STORE"
    storage.mkdir()

    am = AssetManager()

    url = f"lan://my_name@{storage.absolute()}"
    asset_index = am._get_index(url)
    assert asset_index.__class__.__name__ == "LANAssetIndex"
    assert asset_index.username == "my_name"

    url = "memory://my_name@/my_index_ID"
    asset_index = am._get_index(url)
    assert asset_index.__class__.__name__ == "MemoryAssetIndex"
    assert asset_index.username == "my_name"

    if 0:  # not implemented yet
        url = "http://my_name:my_password@my_asset_store:9999"
        asset_index = am._get_index(url)
        assert asset_index._impl.__class__.__name__ == "HTTPAssetStoreImpl"
        assert asset_index._impl.username == "my_name"
        assert asset_index._impl.password == "my_password"
        assert asset_index._impl.port == "9999"

    if 0:  # not implemented yet
        url = "s3://my_api_key@hotname.ext/"
        asset_index = am._get_index(url)
        assert asset_index._impl.__class__.__name__ == "HTTPAssetStoreImpl"
        assert asset_index._impl.__class__.api_key == "my_api_key"


def test_asset_manager_default_index():
    am = AssetManager()
    default_index_url = "lan://my_project/asset_index"
    am.set_default_index(default_index_url)

    assert am._get_index(None).url == default_index_url


def test_asset_manager_index_alias():
    aliases = (
        ("PROJ", "lan://my_project/asset_index"),
        ("LIB", "lan://my_studio/asset_lib"),
        ("DEV", "lan://dev/asset_index"),
        ("Custom Alias", "lan://my_very_own_assets/index"),
    )

    am = AssetManager()
    for alias, index_url in aliases:
        am.set_index_alias(alias, index_url)

    for alias, index_url in aliases:
        assert am._get_index(alias).url == index_url


def test_asset_manager_reduce():
    am = AssetManager()
    index_url = "memory://test_index"
    am.set_default_index(index_url)

    # populate the memory index for tests:

    index = am._get_index(index_url)

    asset_name = "CHARS/Bob/Bob-Actor.ma"
    for version in "1 1.1 1.1.1 1.1.2 1.2 1.2.1 1.2.2 1.3 1.3.1 1.4 2".split(" "):
        index.ingest_content(
            f"This is {asset_name} content for V{version}.",
            asset_name,
            version,
            "testing AssetManager.reduce()",
        )

    flag_to_version = (
        ("cool", "1.2"),
        ("better", "1.3"),
        ("Blessed", "1.4"),
    )
    for flag, version in flag_to_version:
        index.flag_version(asset_name, version, flag)

    # reductions to test:
    requirement_to_reduced = (
        # using flags:
        (Requirement.build_flag(asset_name, "Blessed"), asset_name + "==1.4"),
        (
            Requirement.build_flag(asset_name, index.DYNAMIC_FLAGS.HEAD),
            asset_name + "==2",
        ),
        (f"{asset_name}@Head", asset_name + "==2"),
        (f"{asset_name}@cool", asset_name + "==1.2"),
        (f"{asset_name}@better", asset_name + "==1.3"),
        # already reduced (concrete requirements):
        (f"{asset_name}==1.1.2", f"{asset_name}==1.1.2"),
        (f"{asset_name}==1.2", f"{asset_name}==1.2"),
        (f"{asset_name}==1", f"{asset_name}==1"),
        # pattern (abstract requirement):
        (f"{asset_name}~=1.*", f"{asset_name}==1.4"),
        (f"{asset_name}~=1.2.*", f"{asset_name}==1.2.2"),
        (f"{asset_name}~=1.3.*", f"{asset_name}==1.3.1"),
        (f"{asset_name}~=2.*", f"{asset_name}==2"),
    )

    # test it
    reduced = list(am.reduce(*[req for req, red in requirement_to_reduced]))
    assert len(reduced) == len(requirement_to_reduced)

    for (req, target), red in zip(requirement_to_reduced, reduced):
        assert red == target, f"reducing {req}, expected {target} but got {red}"


def test_asset_manager_publish_LANAssetIndex(tmp_path):
    asset_storage = tmp_path / "ASSET_STORE"
    checkout_path = tmp_path / "Assets"
    am = AssetManager()
    index_url = f"lan://{asset_storage}"
    am.set_default_index(index_url)
    index = am._get_index(index_url)
    _do_test_asset_manager_publish(am, checkout_path)


def test_asset_manager_publish_MemoryAssetIndex(tmp_path):
    checkout_path = tmp_path / "Assets"
    am = AssetManager()
    index_url = "memory://test_index"
    am.set_default_index(index_url)
    index = am._get_index(index_url)
    _do_test_asset_manager_publish(am, checkout_path)


def _do_test_asset_manager_publish(am: AssetManager, checkout_path: Path):
    # populate the memory index for tests:
    index = am._get_index(None)

    def content_for_version(asset_name, version, with_edits):
        content = f"This is {asset_name} content for V{version}."
        if with_edits:
            content += "\nAnd these are some edits..."
        return content

    asset_name = "CHARS/Bob/Bob-Actor.ma"
    version = 1
    index.ingest_content(
        content_for_version(asset_name, version, False),
        asset_name,
        version,
        "testing AssetManager.publish()",
    )
    index.add_version_requirements(
        asset_name, version, ["SomeDependency@Blessed", "AnotherOne@Head"]
    )
    flags_to_bump = {"Blessed", "ToReview"}
    flags_to_leave = {"JustInCase", "Bof"}
    for flag in flags_to_bump | flags_to_leave:
        index.flag_version(asset_name, version, flag)

    # Now do our tests:

    # install as editable, edit, then publish
    install_requirement = f"{asset_name}=={version}+edit"
    am.install_assets(checkout_path, install_requirement)
    asset_path = am.install_path(checkout_path, install_requirement)
    with open(asset_path, "w") as fp:
        fp.write(content_for_version(asset_name, version, with_edits=True))

    # Publish:
    # we use an int version in this case, so we don't use bump args:
    new_requirement = am.publish(
        checkout_path, install_requirement, "Way better version !", flags=flags_to_bump
    )
    # The returned requirement is the installed version, it should have edit since
    # you edit before publishing:
    new_requirement = Requirement(new_requirement)  # coerce
    new_version = new_requirement.version
    assert new_requirement.has_edit
    assert new_requirement.is_concrete
    assert new_version == "2" + Requirement.EDIT_SUFFIX
    # the head sould point to our new version:
    assert list(am.reduce(asset_name + "@Head+edit")) == [
        asset_name + "==" + new_version
    ]

    # This is private inspection, don't do it in your code !!
    index = am._get_index(None)
    editless_new_version = new_requirement.editless_version
    assert index.has_version(asset_name, editless_new_version)
    for flag in flags_to_bump:
        assert index.get_flag_version(asset_name, flag) == editless_new_version
    for flag in flags_to_leave:
        assert index.get_flag_version(asset_name, flag) == str(version)
    md1 = index.get_version_metadata(asset_name, version)
    md2 = index.get_version_metadata(asset_name, editless_new_version)
    assert md1.requirements == md2.requirements
    assert md1.custom_data == md2.custom_data


def _create_versions(
    index: AssetIndex, asset_name: str, version: str, flags: set[str], *dependencies
):
    index.ingest_content("blah...", asset_name, version, "testing resolve dependencies")
    for flag in flags:
        index.flag_version(asset_name, version, flag)
    index.add_version_requirements(asset_name, version, dependencies)


def test_asset_manager_resolve_dependencies():
    am = AssetManager()
    default_flag = "VALIDATED"
    am.set_default_flag(default_flag)
    index_url = "memory://test_index"
    am.set_default_index(index_url)
    index = am._get_index(index_url)

    # Let's populate the index with these dependencies:
    # Mod <-- Lookdev <----------------------------- Lighting
    #     <-------------- Rig <-- Actor <-- Anim <--/ /
    #     <-------------- UVs <--/                   /
    #                         <---------------------/

    asset_index_create_versions(
        index, "Lighting", 12, {}, "Lookdev@Blessed", "Anim", "UVs"
    )
    asset_index_create_versions(index, "Anim", 1, {default_flag}, "Actor@Blessed")
    asset_index_create_versions(index, "Actor", 1, {"Blessed"}, "Rig@Head", "UVs")
    asset_index_create_versions(index, "Rig", 18, {}, "Mod")
    asset_index_create_versions(index, "UVs", 22, {default_flag}, "Mod")
    asset_index_create_versions(index, "Lookdev", 12, {"Blessed"}, "Mod")
    asset_index_create_versions(index, "Mod", 13, {default_flag})

    # lets resolve on different assets and check the results:

    deps = am.resolve_dependencies(["Lighting==12"])
    expected = {
        "Lighting==12",
        "Anim==1",
        "Actor==1",
        "Rig==18",
        "UVs==22",
        "Lookdev==12",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["Anim"])
    expected = {
        "Anim==1",
        "Actor==1",
        "Rig==18",
        "UVs==22",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["Actor@Blessed"])
    expected = {
        "Actor==1",
        "Rig==18",
        "UVs==22",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["Rig==18"])
    expected = {
        "Rig==18",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["UVs@VALIDATED"])
    expected = {
        "UVs==22",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["Lookdev@Blessed"])
    expected = {
        "Lookdev==12",
        "Mod==13",
    }
    assert deps == expected

    deps = am.resolve_dependencies(["Mod@VALIDATED"])
    expected = {
        "Mod==13",
    }
    assert deps == expected
